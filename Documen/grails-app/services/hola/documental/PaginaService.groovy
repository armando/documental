package hola.documental
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp

import javax.imageio.ImageIO;

import grails.transaction.Transactional

import org.apache.pdfbox.util.PDFTextStripper
import org.apache.pdfbox.util.PDFMergerUtility
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDDocumentCatalog
import org.im4java.core.ConvertCmd
import org.im4java.core.IMOperation


import com.sun.org.apache.bcel.internal.generic.RETURN;

@Transactional
class PaginaService {
	def parse(File f, boolean replace) {
		def stripper= new PDFTextStripper()
		log.debug("Procesando ${f.absolutePath}")
		def m= ( f.name =~ /([0-9]{2})([0-9])([0-9])([0-9]{6})([0-9])([0-9])([0-9]{4})([0-9])/ )
		if (m) {
			def pagina= new Pagina()
			pagina.codigo= m.group(0)
			pagina.publicacion= Publicacion.get(m.group(1))
			pagina.versionE= m.group(2).toInteger()
			pagina.publicada= m.group(3)=="1"
			pagina.edicion= Ediciones.findByPublicacionAndNumero(pagina.publicacion, m.group(4).toInteger())
			pagina.tomos= m.group(5).toInteger()
			pagina.tipo= m.group(6).toInteger()
			pagina.numero= m.group(7).toInteger()
			pagina.versionP= m.group(8).toInteger()
			//RandomAccess scratchFile = new RandomAccessFile(File.createTempFile("pdfbox-", ".tmp"), "rw");
			//def pagpdf = PDDocument.loadNonSeq(f, scratchFile)

			if (!pagina.edicion) {
				log.error("Error: no existe edicion: ${m.group(4)}")
			}

			def oldpag= Pagina.findByPublicacionAndEdicionAndNumeroAndVersionP(pagina.publicacion, pagina.edicion, pagina.numero, pagina.versionP)

			if (!oldpag || replace) {
				try {
					def pagpdf= PDDocument.load(f)
					pagina.titular= (pagina.numero==1) ? pagina.edicion?.nombre : ""
					pagina.contenidoPDF= stripper.getText(pagpdf).replaceAll("\000", "")
					pagina.contenido= pagina.contenidoPDF.replaceAll("-\\n","")
					pagina.path= f.name
					pagina.file= f.absolutePath
					if (pagina.validate()){
						if (replace) {
							oldpag.delete()
						}
						pagina.save()
					}
					else{
						pagina.errors.each { println it }
					}
					pagpdf.close()
					log.info("${pagina.edicion}: ${pagina.numero} de ${f.absolutePath}")
				}
				catch(Exception e) {
					log.error(e)
				}
			}
		}
		else {
			log.error("No se puede procesar ${f.absolutePath}")
		}
	}

	def merge(Ediciones e, Integer inicio, Integer fin, OutputStream out) {
		def pdf= new PDFMergerUtility()
		def pagina

		pdf.setDestinationStream(out)

		for (i in inicio..fin) {
			pagina= Pagina.findByEdicionAndNumero(e, i)
			if (pagina) {
				log.debug("Para pdf: ${e} ${i}")
				pdf.addSource(pagina.file)
			}
		}
		pdf.mergeDocuments()
	}

	def mergepdf(Ediciones ed, Integer inicio, Integer fin, File f) {
		def pdf= new PDDocument()
		def pagina
		def docpdf= []
		if (inicio==1 && inicio!=fin) inicio=0

		for (i in inicio..fin) {
			pagina= Pagina.findByEdicionAndNumero(ed, i)
			if (pagina) {
				docpdf[i]= PDDocument.load(pagina.file)
				//log.debug("Para pdf: ${e} ${i} ${docpdf[i].getDocumentCatalog().getAllPages().size()}")
				pdf.importPage(docpdf[i].getDocumentCatalog().getAllPages().get(0))
			}
			else pdf.addPage(new PDPage())
		}
		pdf.getDocumentCatalog().setPageLayout(PDDocumentCatalog.PAGE_LAYOUT_TWO_COLUMN_LEFT)
		pdf.save(f)
		pdf.close()
		for (i in inicio..fin)
			if (docpdf[i]) docpdf[i].close()
	}

	def cachepdf(Ediciones ed, Integer p1, Integer p2) {
		//def ed= Ediciones.findByNumero(e)
		if (ed) {
			def parent= "/var/cache/pdf/${ed.numero}"

			def dir= new File(parent)
			if (!dir.exists()) dir.mkdir()

			def file= new File(dir, filename(ed.numero, p1, p2, 0, "pdf"))
			mergepdf(ed, p1, p2, file)
		}
	}

	def cacheimgs(Ediciones ed, Pagina pag, String type, String format, Integer width) {
		ConvertCmd cmd = new ConvertCmd()
		cmd.setSearchPath("/usr/local/bin")
		def num= pag.numero

		def p1= (num % 2 == 0) ? num : num-1
		def p2= (num % 2 == 0) ? num+1 : num

		def pag1= Pagina.findByEdicionAndNumero(ed, p1)
		def pag2= Pagina.findByEdicionAndNumero(ed, p2)

		def dir= new File("/var/cache/pdf/${ed.numero}")
		if (!dir.exists()) dir.mkdir()
		def file= new File(dir, filename(ed.numero, pag1?.numero, pag2?.numero, width, format))

		log.debug("Procesando ${pag1?.file} ${pag2?.file} ${file.absolutePath}")

		if (type== "image") {
			IMOperation op = new IMOperation()
			if (pag1) op.addImage()
			if (pag2) op.addImage()
			op.p_append()
			op.background("white")
			op.flatten()
			if (width) op.resize(pag1 && pag2 ? width : (width/2).toInteger())
			op.addImage()
			if (pag1 && pag2)
				cmd.run(op, pag1?.file, pag2?.file, file.absolutePath)
			else {
				cmd.run(op, pag1?pag1.file:pag2.file, file.absolutePath)
			}
			/*/
			 PDDocument pdfpages= PDDocument.load(f)
			 PDPage pdfpage= pdfpages.getDocumentCatalog().getAllPages().get(0)
			 BufferedImage im= pdfpage.convertToImage()
			 log.debug("Generado caché para ${base}")
			 ImageIO.write(im, "jpg", new File("${base}.jpg"))
			 pdfpages.close()
			 /*/
		}
		else {
			mergepdf(ed, p1, p2, file)
		}
	}

	public static String filename(Integer e, Integer p1, Integer p2, Integer width, String format) {
		def tag1= p1?:""
		def tag2= p2?:""

		width ? "-"+width : ""

		return "${e}-${tag1}${p1&&p2?'_':''}${tag2}${width ? '-'+width : ''}.${format}"
	}

	public static BufferedImage scale(BufferedImage image, int width, int height) {
		if(image != null) {
			int imageWidth  = image.getWidth()
			int imageHeight = image.getHeight()

			double scaleX = (double)width/imageWidth
			double scaleY = (double)height/imageHeight
			AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaleX, scaleY)
			AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);
			return bilinearScaleOp.filter(
			image,
			new BufferedImage(width, height, image.getType()))
		}
	}
}
