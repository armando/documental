package hola.documental


class TextService {
	def oddchars_o= "aeiounAEIOUN"
	def oddchars_t= "áéíóúñÁÉÍÓÚÑ"

	def highlight(String query, String content, Integer context) {
		def hl= [content.find("(([\\S]*\\s){0,${context*2}})"){match -> match[1]+"..."}]
		if (!query) {
			return hl
		}
		def regex= genregexp(query)
		def highlight= content.findAll("(([\\S]*\\s){0,${context}})("+regex+")(([\\S]*\\s){0,${context}})"){match -> "..."+match[1]+"<em>"+match[3]+"</em>"+match[4]+"..."}
		return highlight.size()>0 ? highlight : hl.head()
	}

	def genregexp(String query) {
		def sb= new StringBuffer()

		query.toCharArray().each{
			sb.append("[")
			if (Character.isWhitespace(it)) {
				sb.append("\\s")
			}
			else {
				sb.append(it).append(to_odd(it))
				def it_o= Character.isUpperCase(it)?it.toLowerCase():it.toUpperCase()
				sb.append(it_o).append(to_odd(it_o))
			}
			sb.append("]")
		}

		return sb.toString()
	}
	
	def to_odd(Character c) {
		def odd= oddchars_o.indexOf(c.toString())
		return (odd >=0) ? oddchars_t[odd] : ''
	}
}
