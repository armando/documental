
<%@ page import="hola.administracion.Seguro" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'seguro.label', default: 'Seguro')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="search" action="search"><g:message code="default.search.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'seguro.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="poliza" title="${message(code: 'seguro.poliza.label', default: 'Poliza')}" />
                        
                            <g:sortableColumn property="fecha" title="${message(code: 'seguro.fecha.label', default: 'Fecha')}" />
                        
                            <g:sortableColumn property="vencimiento" title="${message(code: 'seguro.vencimiento.label', default: 'Vencimiento')}" />
                        
                            <g:sortableColumn property="clase" title="${message(code: 'seguro.clase.label', default: 'Clase')}" />
                        
                            <g:sortableColumn property="lugar" title="${message(code: 'seguro.lugar.label', default: 'Lugar')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${seguroInstanceList}" status="i" var="seguroInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${seguroInstance.id}">${fieldValue(bean: seguroInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: seguroInstance, field: "poliza")}</td>
                        
                            <td><g:formatDate date="${seguroInstance.fecha}" /></td>
                        
                            <td>${fieldValue(bean: seguroInstance, field: "vencimiento")}</td>
                        
                            <td>${fieldValue(bean: seguroInstance, field: "clase")}</td>
                        
                            <td>${fieldValue(bean: seguroInstance, field: "lugar")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${seguroInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
