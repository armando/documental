

<%@ page import="hola.administracion.Seguro" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'seguro.label', default: 'Seguro')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${seguroInstance}">
            <div class="errors">
                <g:renderErrors bean="${seguroInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${seguroInstance?.id}" />
                <g:hiddenField name="version" value="${seguroInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="poliza"><g:message code="seguro.poliza.label" default="Poliza" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: seguroInstance, field: 'poliza', 'errors')}">
                                    <g:textField name="poliza" value="${seguroInstance?.poliza}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="fecha"><g:message code="seguro.fecha.label" default="Fecha" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: seguroInstance, field: 'fecha', 'errors')}">
                                    <g:datePicker name="fecha" precision="day" value="${seguroInstance?.fecha}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="vencimiento"><g:message code="seguro.vencimiento.label" default="Vencimiento" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: seguroInstance, field: 'vencimiento', 'errors')}">
                                    <g:textField name="vencimiento" value="${seguroInstance?.vencimiento}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="clase"><g:message code="seguro.clase.label" default="Clase" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: seguroInstance, field: 'clase', 'errors')}">
                                    <g:textField name="clase" value="${seguroInstance?.clase}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="lugar"><g:message code="seguro.lugar.label" default="Lugar" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: seguroInstance, field: 'lugar', 'errors')}">
                                    <g:textField name="lugar" value="${seguroInstance?.lugar}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
