
<%@ page import="hola.administracion.Registro" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'registro.label', default: 'Registro')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:form method="post" >
              <g:textField name="query" value="${query}" />
              <span class="button"><g:actionSubmit class="search" action="search" value="${message(code: 'default.button.search.label', default: 'Search')}"/></span>
            </g:form>
            
            <g:if test="${registroInstanceTotal>0}">
            <h3>${registroInstanceTotal} resultados</h3>
            </g:if>
            
            
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'registro.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="departamento" title="${message(code: 'registro.departamento.label', default: 'Departamento')}" />
                        
                            <g:sortableColumn property="fecha" title="${message(code: 'registro.fecha.label', default: 'Fecha')}" />
                        
                            <g:sortableColumn property="destinatario" title="${message(code: 'registro.destinatario.label', default: 'Destinatario')}" />
                        
                            <g:sortableColumn property="remitente" title="${message(code: 'registro.remitente.label', default: 'Remitente')}" />
                        
                            <g:sortableColumn property="concepto" title="${message(code: 'registro.concepto.label', default: 'Concepto')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${registroInstanceList}" status="i" var="registroInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${registroInstance.id}">${fieldValue(bean: registroInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: registroInstance, field: "departamento")}</td>
                        
                            <td><g:formatDate date="${registroInstance.fecha}" /></td>
                        
                            <td>${fieldValue(bean: registroInstance, field: "destinatario")}</td>
                        
                            <td>${fieldValue(bean: registroInstance, field: "remitente")}</td>
                        
                            <td>${fieldValue(bean: registroInstance, field: "concepto")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${registroInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
