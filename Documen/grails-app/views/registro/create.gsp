

<%@ page import="hola.administracion.Registro" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'registro.label', default: 'Registro')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${registroInstance}">
            <div class="errors">
                <g:renderErrors bean="${registroInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="departamento"><g:message code="registro.departamento.label" default="Departamento" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registroInstance, field: 'departamento', 'errors')}">
                                    <g:textField name="departamento" value="${registroInstance?.departamento}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fecha"><g:message code="registro.fecha.label" default="Fecha" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registroInstance, field: 'fecha', 'errors')}">
                                    <g:datePicker name="fecha" precision="day" value="${registroInstance?.fecha}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="destinatario"><g:message code="registro.destinatario.label" default="Destinatario" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registroInstance, field: 'destinatario', 'errors')}">
                                    <g:textField name="destinatario" value="${registroInstance?.destinatario}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="remitente"><g:message code="registro.remitente.label" default="Remitente" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registroInstance, field: 'remitente', 'errors')}">
                                    <g:textField name="remitente" value="${registroInstance?.remitente}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="concepto"><g:message code="registro.concepto.label" default="Concepto" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registroInstance, field: 'concepto', 'errors')}">
                                    <g:textField name="concepto" value="${registroInstance?.concepto}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="year"><g:message code="registro.year.label" default="Year" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registroInstance, field: 'year', 'errors')}">
                                    <g:textField name="year" value="${fieldValue(bean: registroInstance, field: 'year')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nueva_fecha"><g:message code="registro.nueva_fecha.label" default="Nuevafecha" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registroInstance, field: 'nueva_fecha', 'errors')}">
                                    <g:datePicker name="nueva_fecha" precision="day" value="${registroInstance?.nueva_fecha}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
