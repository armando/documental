
<%@ page import="hola.documental.Archivo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'archivo.label', default: 'Archivo')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
             <style type="text/css">
<!--
div.scroll {
height: 200px;
width: 300px;
overflow: auto;
border: 1px solid #666;
padding: 8px;
}

-->
</style> 
    </head>
    <body>
      
 
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            
            <g:form method="post" >
              <g:select name="zona" from="${['GARAJE','GALERAS','TODO']}" value="${zona}"/>
              Todo <g:textField name="query" value="${query}" /><br/>
              Carpeta <g:textField name="carpeta" value="${carpeta}" />
              Conexiones <g:textField name="conexiones" value="${conexiones}" />
              Acontecimiento <g:textField name="acontecimiento" value="${acontecimiento}" /><br/>
              <span class="button"><g:actionSubmit class="search" action="search" value="${message(code: 'default.button.search.label', default: 'Search')}"/></span>
            </g:form>
            
            <g:if test="${archivoInstanceTotal>0}">
            <h3>${archivoInstanceTotal} resultados</h3>
            </g:if>
            
            <div class="listres">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'archivo.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="carpeta" title="${message(code: 'archivo.carpeta.label', default: 'Carpeta')}" />
                        
                            <g:sortableColumn property="conexiones" title="${message(code: 'archivo.conexiones.label', default: 'Conexiones')}" />
                        
                            <g:sortableColumn property="acontecimiento" title="${message(code: 'archivo.acontecimiento.label', default: 'Acontecimiento')}" />
                            
                            <g:sortableColumn property="zona" title="${message(code: 'archivo.zona.label', default: 'Zona')}" />
                        
                            <g:sortableColumn property="letra" title="${message(code: 'archivo.letra.label', default: 'Letra')}" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${archivoInstanceList}" status="i" var="archivoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${archivoInstance.id}">${fieldValue(bean: archivoInstance, field: "id")}</g:link></td>
                        
                            <td>
${fieldValue(bean: archivoInstance, field: "carpeta")}
                            </td>
                                                
                            <td><div class="scroll">
                                <g:if test="${conexiones}">
${fieldValue(bean: archivoInstance, field: "conexiones").replaceAll('\n', '<br/>').replaceAll(conexiones,'<strong>'+conexiones+'</strong>')}
                                </g:if>
                                <g:else>
                                  ${fieldValue(bean: archivoInstance, field: "conexiones").replaceAll('\n', '<br/>')}
                                  </g:else>
                              </div>
</td>

                        
                            <td> Acontecimiento ${acontecimiento}
<div class="scroll">
                                <g:if test="${acontecimiento}">
${fieldValue(bean: archivoInstance, field: "acontecimiento").replaceAll('\n', '<br/>').replaceAll(acontecimiento,'<strong>'+acontecimiento+'</strong>')}
                                </g:if>
  <g:else>
    ${fieldValue(bean: archivoInstance, field: "acontecimiento").replaceAll('\n', '<br/>')}
    </g:else>
</div></td>

                            <td>${fieldValue(bean: archivoInstance, field: "zona")}</td>
                        
                                                        <td>
${fieldValue(bean: archivoInstance, field: "letra")}
                              </td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${archivoInstanceTotal}" action="search" params="${params}" />
            </div>
        </div>
    </body>
</html>
