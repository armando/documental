

<%@ page import="hola.documental.Archivo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'archivo.label', default: 'Archivo')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
          <span class="menuButton"><g:link class="list" action="list"><g:message code="default.home.label"/></g:link></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${archivoInstance}">
            <div class="errors">
                <g:renderErrors bean="${archivoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${archivoInstance?.id}" />
                <g:hiddenField name="version" value="${archivoInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                          
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="carpeta"><g:message code="archivo.zona.label" default="Zona" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'zona', 'errors')}">
                                    <g:select from="${['ARCHIVO','GARAJE']}" name="zona" value="${archivoInstance?.zona}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="carpeta"><g:message code="archivo.carpeta.label" default="Carpeta" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'carpeta', 'errors')}">
                                    <g:textField size="40" maxlength="255" name="carpeta" value="${archivoInstance?.carpeta}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="letra"><g:message code="archivo.letra.label" default="Letra" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'letra', 'errors')}">
                                    <g:textField size="40" maxlength="255" name="letra" value="${archivoInstance?.letra}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="conexiones"><g:message code="archivo.conexiones.label" default="Conexiones" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'conexiones', 'errors')}">
                                    <g:textArea maxlength="512" name="conexiones" value="${archivoInstance?.conexiones}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="acontecimiento"><g:message code="archivo.acontecimiento.label" default="Acontecimiento" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'acontecimiento', 'errors')}">
                                    <g:textArea maxlength="512" name="acontecimiento" value="${archivoInstance?.acontecimiento}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
