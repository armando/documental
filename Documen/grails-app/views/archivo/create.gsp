

<%@ page import="hola.documental.Archivo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'archivo.label', default: 'Archivo')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${archivoInstance}">
            <div class="errors">
                <g:renderErrors bean="${archivoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                          
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="carpeta"><g:message code="archivo.zona.label" default="Zona" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'zona', 'errors')}">
                                    <g:select from="${['GARAJE','GALERAS']}" name="zona" value="${archivoInstance?.zona}" />
                                </td>
                            </tr>
                        
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="carpeta"><g:message code="archivo.carpeta.label" default="Carpeta" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'carpeta', 'errors')}">
                                    <g:textField name="carpeta" size="40" maxlength="255" value="${archivoInstance?.carpeta}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="letra"><g:message code="archivo.letra.label" default="Letra" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'letra', 'errors')}">
                                    <g:textField name="letra" size="40" maxlength="255" value="${archivoInstance?.letra}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="conexiones"><g:message code="archivo.conexiones.label" default="Conexiones" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'conexiones', 'errors')}">
                                    <g:textArea name="conexiones" maxlength="512" value="${archivoInstance?.conexiones}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="acontecimiento"><g:message code="archivo.acontecimiento.label" default="Acontecimiento" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: archivoInstance, field: 'acontecimiento', 'errors')}">
                                    <g:textArea name="acontecimiento" maxlength="512" value="${archivoInstance?.acontecimiento}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
