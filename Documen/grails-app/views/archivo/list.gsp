
<%@ page import="hola.documental.Archivo" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'archivo.label', default: 'Archivo')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="search" action="search"><g:message code="default.search.label" args="[entityName]" /></g:link></span>

        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'archivo.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="carpeta" title="${message(code: 'archivo.carpeta.label', default: 'Carpeta')}" />
                        
                            <g:sortableColumn property="letra" title="${message(code: 'archivo.letra.label', default: 'Letra')}" />
                        
                            <g:sortableColumn property="conexiones" title="${message(code: 'archivo.conexiones.label', default: 'Conexiones')}" />
                        
                            <g:sortableColumn property="acontecimiento" title="${message(code: 'archivo.acontecimiento.label', default: 'Acontecimiento')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${archivoInstanceList}" status="i" var="archivoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${archivoInstance.id}">${fieldValue(bean: archivoInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: archivoInstance, field: "carpeta")}</td>
                        
                            <td>${fieldValue(bean: archivoInstance, field: "letra")}</td>
                        
                            <td>${fieldValue(bean: archivoInstance, field: "conexiones")}</td>
                        
                            <td>${fieldValue(bean: archivoInstance, field: "acontecimiento")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${archivoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
