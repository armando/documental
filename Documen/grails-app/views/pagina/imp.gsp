<!--
  To change this template, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Importación</title>
  </head>
  <body>
  <g:form method="post" action="imp">
									Directorio: <g:textField name="dir" size="50" maxSize="50" value="${params.dir}"/><br/>
									Recurse: <g:checkBox name="recurse" value="${params.recurse}"/><br/>
									Replace: <g:checkBox name="replace" value="${params.replace}"/><br/>
									Inicio: <g:textField name="inicio" size="3" maxSize="3" value="${params.inicio}"/><br/>
									Fin: <g:textField name="fin" size="3" maxSize="3" value="${params.fin}"/><br/>
									<span class="button"> 
									<g:actionSubmit class="imp"
											action="imp" value="Importar" />
									</span>&nbsp;
								</g:form>
								<g:if test="${completa}">
    <h1>Importación completa</h1>
    </g:if>
  </body>
</html>
