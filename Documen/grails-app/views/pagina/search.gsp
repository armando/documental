
<%@page import="java.util.regex.Matcher"%>
<%@ page import="hola.documental.Pagina"%>
<%@ page import="hola.documental.Publicacion"%>
<%@ page import="hola.documental.Ediciones"%>
<g:set var="text" bean="textService" />
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'pagina.label', default: 'Pagina')}" />
<title><g:message code="default.search.label"
		args="[entityName]" /></title>
<script type="text/javascript"
	src="${resource(dir: 'js/prototype', file: 'prototype.js')}"></script>
<script type="text/javascript"
	src="${resource(dir: 'js/prototype', file: 'scriptaculous.js?load=effects')}"></script>
<script type="text/javascript"
	src="${resource(dir: 'js', file: 'lightwindow.js')}"></script>
<link rel="stylesheet"
	href="${resource(dir: 'css', file: 'lightwindow.css')}" type="text/css"
	media="screen" />
</head>
<body>

<form name="doublecombo">
<p>
<g:select name="publicacion" from="${Publicacion.list()}"
			value="${params.publicacion}" optionKey="id" optionValue="nombre"
			onChange="redirect(this.options.selectedIndex)"/>

<select name="inicio" size="1">
</select>
<select name="fin" size="1">
</select>
<input type="button" name="test" value="Go!"
onClick="go()">
</p>

<script>
<!--

/*
Double Combo Script Credit
By JavaScript Kit (www.javascriptkit.com)
Over 200+ free JavaScripts here!
*/

var groups= ${Publicacion.count()}
var group=new Array(groups)
for (i=0; i<groups; i++)
	group[i]=new Array()

<g:each var="publicacion" status="i" in="${Publicacion.listOrderByOrden(order: 'asc')}">
	<g:set var="pub" value="${publicacion}"/>
	<g:each var="edicion" status="j" in="${Ediciones.findAllByPublicacionAndFechaPortadaLessThan(pub,new Date()+7, [sort:'fechaPortada', order:'asc'])}">
group[${i}][${j}]=new Option("${edicion}","${edicion.id}")
	</g:each>
</g:each>
var temp=document.doublecombo.fin
var temp2= document.doublecombo.inicio

function redirect(x){
	for (m=temp.options.length-1;m>0;m--){
		temp.options[m]=null
		temp2.options[m]=null
	}
	for (i=0;i<group[x].length;i++){
		temp.options[i]=new Option(group[x][i].text,group[x][i].value)
		temp2.options[i]=new Option(group[x][i].text,group[x][i].value)
	}
	temp.options[0].selected=true
	temp.options[group[x].length-1].selected=true
}

function go(){
	location=temp.options[temp.selectedIndex].value
}

redirect(0)
//-->
</script>

</form>










	<h1>PDF de imprenta</h1>

	<g:form method="post" action="search">
	    Buscar en <g:select name="publicacion" from="${Publicacion.list()}"
			value="${params.publicacion}" optionKey="id" optionValue="nombre"
			noSelection="${['null':'TODO']}" /> 
        Edición: <g:textField name="edicion" size="5"
			value="${params.edicion}" />
		Número: <g:textField name="pagina" size="5"
			value="${params.numero}" />
        Desde: <g:select name="start" from="${Ediciones.list()}"
			value="${params.start}" optionKey="id"
			noSelection="${firstEdition}" />
        Hasta: <g:select name="end" from="${Ediciones.list()}"
			value="${params.end}" optionKey="id"
			noSelection="${lastEdition}" />
		<br />
		Texto: <g:textField name="q" size="80" value="${params.q}" />
		<br />


		<span class="button"><g:actionSubmit action="search"
				class="search"
				value="${message(code: 'default.button.search.label', default: 'Buscar')}" /></span>
	</g:form>

	<div id="search-pagina" class="content scaffold-list" role="main">
		<h1>
			<g:message code="default.searchfacet.label" args="[entityName]"
				default="Resultados" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>


		<g:if test="${paginaList.size()>0}">
			${paginaTotal} páginas	
			<!--  
			<g:form action="search">
	
		<g:hiddenField name="q" value="${params.q}" />
		<g:each in="${facetResults}" var="facet">
			<select name="fq_${facet.name}">
			<option value="" selected>${facet.name.toUpperCase()}</option>
				<g:each in="${facet.values}" var="elem">
					<option value="${facet.name}:${elem.name}">
						${elem.name} (${elem.count})
					</option>
				</g:each>
			</select>
		</g:each>
		<span class="button"><g:actionSubmit action="search" class="search"
				value="${message(code: 'default.button.facet.label', default: 'Filtrar')}" /></span>
	</g:form>
	-->
	
	<div class="pagination">
				<g:paginate total="${paginaTotal}" params="${params}" />
			</div>
			<table>
				<thead>
					<tr>
						<th><g:message code="pagina.edicion.label"
								default="Edición&nbsp;(inicio&#8209;fin)" /></th>

						<g:sortableColumn property="pagina.fecha"
							title="${message(code: 'fecha.label', default: 'Fecha')}" />

						<g:sortableColumn property="pagina.numero"
							title="${message(code: 'pagina.numero.label', default: 'Pág.')}" />

						<th><g:message code="contenido" default="" /></th>

					</tr>
				</thead>
				<tbody>
					<g:each in="${paginaList}" status="i" var="pagina">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

							<td>
								${pagina.publicacion}
						
							<g:form method="post" action="pdf" target="_blank">
									<span class="button"> <g:actionSubmit class="search"
											action="pdf" value="${pagina.edicion}" />
									</span>&nbsp;
								<g:hiddenField name="edicion" value="${pagina.edicion}" />
									<g:textField name="rango" size="7" maxSize="7"
										value="${pagina.numero}" />&nbsp;
								</g:form></td>

							<td><g:formatDate format="dd-MMM-yyyy"
									date="${pagina.fecha}" /></td>

							<td><a
								href="<g:createLink action='view'/>/${pagina.edicion}/${pagina.id}.jpg"
								class="lightwindow" title="${pagina.numero}"> <img
									src="<g:createLink action='view'/>/${pagina.edicion}/${pagina.id}_100.png">
							</a></td>

							<td><g:set var="hl"
									value="${text.highlight(params.q, pagina.contenido, 30)}" /> <g:each
									in="${hl}" var="high">
							...${high}...<br />
								</g:each></td>
						</tr>
					</g:each>
				</tbody>
			</table>


			<div class="pagination">
				<g:paginate total="${paginaTotal}" params="${params}" />
			</div>
		</g:if>
	</div>


</body>
</html>
