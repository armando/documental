<%@ page import="hola.documental.Pagina" %>



<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'contenido', 'error')} ">
	<label for="contenido">
		<g:message code="pagina.contenido.label" default="Contenido" />
		
	</label>
	<g:textField name="contenido" value="${paginaInstance?.contenido}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'contenidoPDF', 'error')} ">
	<label for="contenidoPDF">
		<g:message code="pagina.contenidoPDF.label" default="Contenido PDF" />
		
	</label>
	<g:textField name="contenidoPDF" value="${paginaInstance?.contenidoPDF}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'edicion', 'error')} required">
	<label for="edicion">
		<g:message code="pagina.edicion.label" default="Edicion" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="edicion" name="edicion.id" from="${hola.documental.Ediciones.list()}" optionKey="id" required="" value="${paginaInstance?.edicion?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'numero', 'error')} required">
	<label for="numero">
		<g:message code="pagina.numero.label" default="Numero" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="numero" type="number" value="${paginaInstance.numero}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'path', 'error')} ">
	<label for="path">
		<g:message code="pagina.path.label" default="Path" />
		
	</label>
	<g:textField name="path" value="${paginaInstance?.path}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'publicacion', 'error')} required">
	<label for="publicacion">
		<g:message code="pagina.publicacion.label" default="Publicacion" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="publicacion" name="publicacion.id" from="${hola.documental.Publicacion.list()}" optionKey="id" required="" value="${paginaInstance?.publicacion?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'publicada', 'error')} ">
	<label for="publicada">
		<g:message code="pagina.publicada.label" default="Publicada" />
		
	</label>
	<g:checkBox name="publicada" value="${paginaInstance?.publicada}" />
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'tipo', 'error')} required">
	<label for="tipo">
		<g:message code="pagina.tipo.label" default="Tipo" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="tipo" type="number" value="${paginaInstance.tipo}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'tomos', 'error')} required">
	<label for="tomos">
		<g:message code="pagina.tomos.label" default="Tomos" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="tomos" type="number" value="${paginaInstance.tomos}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'versionE', 'error')} required">
	<label for="versionE">
		<g:message code="pagina.versionE.label" default="Version E" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="versionE" type="number" value="${paginaInstance.versionE}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: paginaInstance, field: 'versionP', 'error')} required">
	<label for="versionP">
		<g:message code="pagina.versionP.label" default="Version P" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="versionP" type="number" value="${paginaInstance.versionP}" required=""/>
</div>

