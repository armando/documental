<div class="large-6 medium-6 columns">
	<label>Desde</label>
	<select name="start">
		<g:each var="edicion" in="${ediciones}" status="i">
			<option value="${edicion.numero}" ${i==0?"selected":""}>
				${edicion}
			</option>
		</g:each>
	</select>
</div>
<div class="large-6 medium-6 columns">
	<label>Hasta</label> 
	<select name="end">
		<g:each var="edicion" in="${ediciones}" status="i">
			<option value="${edicion.numero}"
				${i==ediciones.size()-1?"selected":""}>
				${edicion}
			</option>
		</g:each>
	</select>
</div>