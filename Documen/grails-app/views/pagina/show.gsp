
<%@ page import="hola.documental.Pagina" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pagina.label', default: 'Pagina')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-pagina" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-pagina" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list pagina">
			
				<g:if test="${paginaInstance?.contenido}">
				<li class="fieldcontain">
					<span id="contenido-label" class="property-label"><g:message code="pagina.contenido.label" default="Contenido" /></span>
					
						<span class="property-value" aria-labelledby="contenido-label"><g:fieldValue bean="${paginaInstance}" field="contenido"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.contenidoPDF}">
				<li class="fieldcontain">
					<span id="contenidoPDF-label" class="property-label"><g:message code="pagina.contenidoPDF.label" default="Contenido PDF" /></span>
					
						<span class="property-value" aria-labelledby="contenidoPDF-label"><g:fieldValue bean="${paginaInstance}" field="contenidoPDF"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="pagina.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${paginaInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.edicion}">
				<li class="fieldcontain">
					<span id="edicion-label" class="property-label"><g:message code="pagina.edicion.label" default="Edicion" /></span>
					
						<span class="property-value" aria-labelledby="edicion-label"><g:link controller="ediciones" action="show" id="${paginaInstance?.edicion?.id}">${paginaInstance?.edicion?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="pagina.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${paginaInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.numero}">
				<li class="fieldcontain">
					<span id="numero-label" class="property-label"><g:message code="pagina.numero.label" default="Numero" /></span>
					
						<span class="property-value" aria-labelledby="numero-label"><g:fieldValue bean="${paginaInstance}" field="numero"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.path}">
				<li class="fieldcontain">
					<span id="path-label" class="property-label"><g:message code="pagina.path.label" default="Path" /></span>
					
						<span class="property-value" aria-labelledby="path-label"><g:fieldValue bean="${paginaInstance}" field="path"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.publicacion}">
				<li class="fieldcontain">
					<span id="publicacion-label" class="property-label"><g:message code="pagina.publicacion.label" default="Publicacion" /></span>
					
						<span class="property-value" aria-labelledby="publicacion-label"><g:link controller="publicacion" action="show" id="${paginaInstance?.publicacion?.id}">${paginaInstance?.publicacion?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.publicada}">
				<li class="fieldcontain">
					<span id="publicada-label" class="property-label"><g:message code="pagina.publicada.label" default="Publicada" /></span>
					
						<span class="property-value" aria-labelledby="publicada-label"><g:formatBoolean boolean="${paginaInstance?.publicada}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.tipo}">
				<li class="fieldcontain">
					<span id="tipo-label" class="property-label"><g:message code="pagina.tipo.label" default="Tipo" /></span>
					
						<span class="property-value" aria-labelledby="tipo-label"><g:fieldValue bean="${paginaInstance}" field="tipo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.tomos}">
				<li class="fieldcontain">
					<span id="tomos-label" class="property-label"><g:message code="pagina.tomos.label" default="Tomos" /></span>
					
						<span class="property-value" aria-labelledby="tomos-label"><g:fieldValue bean="${paginaInstance}" field="tomos"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.versionE}">
				<li class="fieldcontain">
					<span id="versionE-label" class="property-label"><g:message code="pagina.versionE.label" default="Version E" /></span>
					
						<span class="property-value" aria-labelledby="versionE-label"><g:fieldValue bean="${paginaInstance}" field="versionE"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${paginaInstance?.versionP}">
				<li class="fieldcontain">
					<span id="versionP-label" class="property-label"><g:message code="pagina.versionP.label" default="Version P" /></span>
					
						<span class="property-value" aria-labelledby="versionP-label"><g:fieldValue bean="${paginaInstance}" field="versionP"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:paginaInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${paginaInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
