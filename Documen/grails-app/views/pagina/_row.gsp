<g:set var="text" bean="textService" />
<%@ page import="hola.documental.Pagina"%>
<%@ page import="hola.documental.Publicacion"%>
<%@ page import="hola.documental.Ediciones"%>


<g:set var="num" value="${pagina.numero}" />
<g:set var="p1" value="${(num % 2 == 0) ? num : num-1}" />
<g:set var="p2" value="${(num % 2 == 0) ? num+1 : num}" />

<g:set var="rango" value="${p1>0?p1+'-'+p2:p2}" />

<td>
 <g:form
		method="post" action="pdf" target="_blank">
		<g:hiddenField name="edicion" value="${pagina.edicion.numero}" />
		<g:hiddenField name="rango"
			value='${pagina.numero}' />&nbsp;
				<g:actionSubmit class="tiny radius alert button"
				action="pdf" value="${ed}-${pagina.numero}" />		
	</g:form>

<g:formatDate format="dd/MM/yyyy"
		date="${pagina.edicion.fechaPortada}" />
 <g:form
		method="post" action="pdf" target="_blank">
		<g:hiddenField name="edicion" value="${pagina.edicion.numero}" />
		<g:textField name="rango" size="7" maxSize="7"
			value='${rango}' />&nbsp;
				<g:actionSubmit class="tiny radius button"
				action="pdf" value="PDF" />
		
	</g:form>
</td>

<td>
<table><tr>
<td style="padding:0; vertical-align:middle;" >
<g:if test="${pagina.numero>1}">
		<g:remoteLink action="row" update="row-${row}"
			params="[pag:pagina.numero-1, ed:ed, q:q, row:row, context:context]">
			<g:img dir="images" file="back-icon.png" width="32" height="32"/>
			</g:remoteLink>
	</g:if>
	</td><td>
	<ul class="clearing-thumbs clearing-feature" data-clearing>

	<g:each var="pag" in="${pags}">
	<li class="${pag==pagina?'clearing-featured-img':''}">
	<a
	href="<g:createLink action='view'/>/${ed}/${pag.id}.png"
	title="${ed}&nbsp;${rango}"> 
	<img src="<g:createLink action='view'/>/${ed}/${pag.id}_200.png"></a>
	</li>
	</if>
	</g:each>
	</ul>
	
</td>
<td style="padding:0; vertical-align:middle;">
 <g:remoteLink action="row" update="row-${row}"
		params="[pag:pagina.numero+1, ed:ed, q:q, row:row, context:context]">
		<g:img dir="images" file="forward-icon.png" width="32" height="32"/>
		</g:remoteLink>
</td>
</tr>
</table>

</td>

<td>
	<div id="panel-${row}" class="panel">
	<h3>${pagina.titular}</h3>
			<g:each in="${hl}">
				<p>${it}</p>
			</g:each>
	</div>
</td>
