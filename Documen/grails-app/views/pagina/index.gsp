
<%@ page import="hola.documental.Pagina" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pagina.label', default: 'Pagina')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-pagina" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-pagina" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="contenido" title="${message(code: 'pagina.contenido.label', default: 'Contenido')}" />
					
						<g:sortableColumn property="contenidoPDF" title="${message(code: 'pagina.contenidoPDF.label', default: 'Contenido PDF')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'pagina.dateCreated.label', default: 'Date Created')}" />
					
						<th><g:message code="pagina.edicion.label" default="Edicion" /></th>
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'pagina.lastUpdated.label', default: 'Last Updated')}" />
					
						<g:sortableColumn property="numero" title="${message(code: 'pagina.numero.label', default: 'Numero')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${paginaInstanceList}" status="i" var="paginaInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${paginaInstance.id}">${fieldValue(bean: paginaInstance, field: "contenido")}</g:link></td>
					
						<td>${fieldValue(bean: paginaInstance, field: "contenidoPDF")}</td>
					
						<td><g:formatDate date="${paginaInstance.dateCreated}" /></td>
					
						<td>${fieldValue(bean: paginaInstance, field: "edicion")}</td>
					
						<td><g:formatDate date="${paginaInstance.lastUpdated}" /></td>
					
						<td>${fieldValue(bean: paginaInstance, field: "numero")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${paginaInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
