
<%@page import="java.util.regex.Matcher"%>
<%@ page import="hola.documental.Pagina"%>
<%@ page import="hola.documental.Publicacion"%>
<%@ page import="hola.documental.Ediciones"%>
<g:set var="text" bean="textService" />
<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
<meta name="layout" content="main">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<g:set var="entityName"
	value="${message(code: 'pagina.label', default: 'Pagina')}" />
<title>Búsqueda de PDF</title>

<script type="text/javascript"
	src="${resource(dir: 'js/vendor', file: 'modernizr.js')}"></script>

<link rel="stylesheet"
	href="${resource(dir: 'css', file: 'foundation.css')}" type="text/css"
	media="screen" />
<link rel="stylesheet"
	href="${resource(dir: 'css', file: 'default.css')}" type="text/css"
	media="screen" />
</head>
<body>
	<g:form method="post" action="busca">
		<div class="row">

			<div class="large-2 medium-2 columns">
				<label>Publicación</label>
				<g:select name="publicacion" from="${Publicacion.list()}"
					value="${params.publicacion}" optionKey="id" optionValue="nombre"
					onchange="${remoteFunction (
                        controller: 'pagina',
                        action: 'range',
                        params: '\'publicacion=\' + this.value',
                        update: 'range'
                )}" />
			</div>
			<div class="large-1 medium-1 columns">
				<label>Edición</label>
				<g:textField name="edicion" size="5" value="${params.edicion}" />
			</div>
			<div class="large-1 medium-1 columns">
				<label>Página</label>
				<g:textField name="pagina" size="5" value="${params.pagina}" />
			</div>
			<div class="large-8 medium-8 columns">
				<div id="range">
					<g:include action="range" params="[publicacion: 1]" />
				</div>
			</div>

		</div>
		<div class="row">
			<div class="large-10 medium-10 columns">
				<g:textField name="q" size="80" value="${params.q}" />
			</div>
			<div class="large-2 medium-2 columns">
				<g:actionSubmit action="busca" class="tiny radius button"
					value="${message(code: 'default.button.search.label', default: 'Buscar')}" />
			</div>

		</div>
	</g:form>

	<div class="row">
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
	</div>

	<div class="row">
		<g:if test="${paginaList?.size()>0}">


			<table>
				<thead>
					<tr>
						<g:sortableColumn property="edicion"
							title="${message(code: 'pagina.edicion.label', default: 'Edición')}"
							params="${params}" />

<!--
						<g:sortableColumn property="numero"
							title="${message(code: 'pagina.numero.label', default: 'Páginas (${paginaTotal})')}"
							params="${params}" />
-->
<th>Páginas (${paginaTotal})</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${paginaList}" status="i" var="pagina">
						<tr id="row-${i}" class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<g:include action="row"
								params="[pag:pagina.numero, ed:pagina.edicion, q:params.q, row:i, context:2]" />

						</tr>
					</g:each>
				</tbody>
			</table>


			<div class="pagination">
				<g:paginate total="${paginaTotal}" params="${params}" />
			</div>
		</g:if>
	</div>


	<script type="text/javascript"
		src="${resource(dir: 'js/vendor', file: 'jquery.js')}"></script>
	<script type="text/javascript"
		src="${resource(dir: 'js', file: 'foundation.min.js')}"></script>
	<script>
      $(document).foundation();
    </script>
</body>
</html>
