

<%@ page import="hola.documental.Hola" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'hola.label', default: 'Hola')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${holaInstance}">
            <div class="errors">
                <g:renderErrors bean="${holaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="numero"><g:message code="hola.numero.label" default="Numero" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'numero', 'errors')}">
                                    <g:textField name="numero" value="${holaInstance?.numero}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="personaje"><g:message code="hola.personaje.label" default="Personaje" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'personaje', 'errors')}">
                                    <g:textField name="personaje" value="${holaInstance?.personaje}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="autor"><g:message code="hola.autor.label" default="Autor" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'autor', 'errors')}">
                                    <g:textField name="autor" value="${holaInstance?.autor}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="observaciones"><g:message code="hola.observaciones.label" default="Observaciones" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'observaciones', 'errors')}">
                                    <g:textField name="observaciones" value="${holaInstance?.observaciones}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="codigo"><g:message code="hola.codigo.label" default="Codigo" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'codigo', 'errors')}">
                                    <g:textField name="codigo" value="${holaInstance?.codigo}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nacion"><g:message code="hola.nacion.label" default="Nacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'nacion', 'errors')}">
                                    <g:textField name="nacion" value="${holaInstance?.nacion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nacimiento"><g:message code="hola.nacimiento.label" default="Nacimiento" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'nacimiento', 'errors')}">
                                    <g:textField name="nacimiento" value="${holaInstance?.nacimiento}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="profesion"><g:message code="hola.profesion.label" default="Profesion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'profesion', 'errors')}">
                                    <g:textField name="profesion" value="${holaInstance?.profesion}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="muerte"><g:message code="hola.muerte.label" default="Muerte" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'muerte', 'errors')}">
                                    <g:textField name="muerte" value="${holaInstance?.muerte}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fecha"><g:message code="hola.fecha.label" default="Fecha" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'fecha', 'errors')}">
                                    <g:datePicker name="fecha" precision="day" value="${holaInstance?.fecha}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="revista"><g:message code="hola.revista.label" default="Revista" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: holaInstance, field: 'revista', 'errors')}">
                                    <g:textField name="revista" value="${holaInstance?.revista}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
