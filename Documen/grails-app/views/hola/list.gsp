
<%@ page import="hola.documental.Hola" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'hola.label', default: 'Hola')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'hola.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="numero" title="${message(code: 'hola.numero.label', default: 'Numero')}" />
                        
                            <g:sortableColumn property="personaje" title="${message(code: 'hola.personaje.label', default: 'Personaje')}" />
                        
                            <g:sortableColumn property="autor" title="${message(code: 'hola.autor.label', default: 'Autor')}" />
                        
                            <g:sortableColumn property="observaciones" title="${message(code: 'hola.observaciones.label', default: 'Observaciones')}" />
                        
                            <g:sortableColumn property="codigo" title="${message(code: 'hola.codigo.label', default: 'Codigo')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${holaInstanceList}" status="i" var="holaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${holaInstance.id}">${fieldValue(bean: holaInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: holaInstance, field: "numero")}</td>
                        
                            <td>${fieldValue(bean: holaInstance, field: "personaje")}</td>
                        
                            <td>${fieldValue(bean: holaInstance, field: "autor")}</td>
                        
                            <td>${fieldValue(bean: holaInstance, field: "observaciones")}</td>
                        
                            <td>${fieldValue(bean: holaInstance, field: "codigo")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${holaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
