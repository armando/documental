

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Articulo</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Articulo List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Articulo</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Articulo</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${articulo}">
            <div class="errors">
                <g:renderErrors bean="${articulo}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${articulo?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="numero">Numero:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:articulo,field:'numero','errors')}">
                                    <input type="text" id="numero" name="numero" value="${fieldValue(bean:articulo,field:'numero')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="pagina">Pagina:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:articulo,field:'pagina','errors')}">
                                    <input type="text" id="pagina" name="pagina" value="${fieldValue(bean:articulo,field:'pagina')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="antetitulo">Antetitulo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:articulo,field:'antetitulo','errors')}">
                                    <input type="text" maxlength="60" id="antetitulo" name="antetitulo" value="${fieldValue(bean:articulo,field:'antetitulo')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="titulo">Titulo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:articulo,field:'titulo','errors')}">
                                    <textarea rows="5" cols="40" name="titulo">${fieldValue(bean:articulo, field:'titulo')}</textarea>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="subtitulo">Subtitulo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:articulo,field:'subtitulo','errors')}">
                                    <input type="text" maxlength="60" id="subtitulo" name="subtitulo" value="${fieldValue(bean:articulo,field:'subtitulo')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="autor">Autor:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:articulo,field:'autor','errors')}">
                                    <input type="text" maxlength="60" id="autor" name="autor" value="${fieldValue(bean:articulo,field:'autor')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="keywords">Keywords:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:articulo,field:'keywords','errors')}">
                                    <input type="text" maxlength="100" id="keywords" name="keywords" value="${fieldValue(bean:articulo,field:'keywords')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
