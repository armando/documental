

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Articulo List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Articulo</g:link></span>
        </div>
        <div class="body">
            <h1>Articulo List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="numero" title="Numero" />
                        
                   	        <g:sortableColumn property="pagina" title="Pagina" />
                        
                   	        <g:sortableColumn property="antetitulo" title="Antetitulo" />
                        
                   	        <g:sortableColumn property="titulo" title="Titulo" />
                        
                   	        <g:sortableColumn property="subtitulo" title="Subtitulo" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${articuloList}" status="i" var="articulo">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${articulo.id}">${fieldValue(bean:articulo, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:articulo, field:'numero')}</td>
                        
                            <td>${fieldValue(bean:articulo, field:'pagina')}</td>
                        
                            <td>${fieldValue(bean:articulo, field:'antetitulo')}</td>
                        
                            <td>${fieldValue(bean:articulo, field:'titulo')}</td>
                        
                            <td>${fieldValue(bean:articulo, field:'subtitulo')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${Articulo.count()}" />
            </div>
        </div>
    </body>
</html>
