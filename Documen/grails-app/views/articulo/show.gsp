

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show Articulo</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Articulo List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Articulo</g:link></span>
        </div>
        <div class="body">
            <h1>Show Articulo</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:articulo, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Numero:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:articulo, field:'numero')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Pagina:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:articulo, field:'pagina')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Antetitulo:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:articulo, field:'antetitulo')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Titulo:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:articulo, field:'titulo')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Subtitulo:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:articulo, field:'subtitulo')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Autor:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:articulo, field:'autor')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Keywords:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:articulo, field:'keywords')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${articulo?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
