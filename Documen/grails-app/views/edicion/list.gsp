
<%@ page import="hola.documental.Edicion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'edicion.label', default: 'Edicion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'edicion.id.label', default: 'Id')}" />
                        
                            <th><g:message code="edicion.publicacion.label" default="Publicacion" /></th>
                   	    
                            <g:sortableColumn property="lastUpdated" title="${message(code: 'edicion.lastUpdated.label', default: 'Last Updated')}" />
                        
                            <g:sortableColumn property="numero" title="${message(code: 'edicion.numero.label', default: 'Numero')}" />
                        
                            <g:sortableColumn property="nombre" title="${message(code: 'edicion.nombre.label', default: 'Nombre')}" />
                        
                            <g:sortableColumn property="fechaPublicacion" title="${message(code: 'edicion.fechaPublicacion.label', default: 'Fecha Publicacion')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${edicionInstanceList}" status="i" var="edicionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${edicionInstance.id}">${fieldValue(bean: edicionInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: edicionInstance, field: "publicacion")}</td>
                        
                            <td><g:formatDate date="${edicionInstance.lastUpdated}" /></td>
                        
                            <td>${fieldValue(bean: edicionInstance, field: "numero")}</td>
                        
                            <td>${fieldValue(bean: edicionInstance, field: "nombre")}</td>
                        
                            <td><g:formatDate date="${edicionInstance.fechaPublicacion}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${edicionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
