
<%@ page import="hola.documental.Edicion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'edicion.label', default: 'Edicion')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${edicionInstance}">
            <div class="errors">
                <g:renderErrors bean="${edicionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="publicacion"><g:message code="edicion.publicacion.label" default="Publicacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: edicionInstance, field: 'publicacion', 'errors')}">
                                    <g:select name="publicacion.id" from="${hola.documental.Publicacion.list()}" optionKey="id" value="${edicionInstance?.publicacion?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="edicion.lastUpdated.label" default="Last Updated" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: edicionInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" precision="day" value="${edicionInstance?.lastUpdated}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="numero"><g:message code="edicion.numero.label" default="Numero" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: edicionInstance, field: 'numero', 'errors')}">
                                    <g:textField name="numero" value="${fieldValue(bean: edicionInstance, field: 'numero')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nombre"><g:message code="edicion.nombre.label" default="Nombre" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: edicionInstance, field: 'nombre', 'errors')}">
                                    <g:textField name="nombre" value="${edicionInstance?.nombre}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaPublicacion"><g:message code="edicion.fechaPublicacion.label" default="Fecha Publicacion" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: edicionInstance, field: 'fechaPublicacion', 'errors')}">
                                    <g:datePicker name="fechaPublicacion" precision="day" value="${edicionInstance?.fechaPublicacion}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fechaPortada"><g:message code="edicion.fechaPortada.label" default="Fecha Portada" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: edicionInstance, field: 'fechaPortada', 'errors')}">
                                    <g:datePicker name="fechaPortada" precision="day" value="${edicionInstance?.fechaPortada}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="edicion.dateCreated.label" default="Date Created" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: edicionInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" precision="day" value="${edicionInstance?.dateCreated}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="repositorio"><g:message code="edicion.repositorio.label" default="Repositorio" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: edicionInstance, field: 'repositorio', 'errors')}">
                                    <g:textField name="repositorio" value="${edicionInstance?.repositorio}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
