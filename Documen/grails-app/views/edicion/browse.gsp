
<%@ page import="hola.documental.Edicion" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'edicion.label', default: 'Edicion')}" />
        <title>${dir}</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>

            <g:link action="parse" params="[dir:dir]">Analizar</g:link>

            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <th><g:message code="edicion.publicacion.label" default="Dir" /></th>
                   	    
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${dirs}" status="i" var="d">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">                          
                            <td><g:link action="browse" params="[dir:dir, subdir:d.getName()]">${d.getName()}</g:link></td>
                        </tr>
                    </g:each>
                    <g:each in="${files}" status="i" var="f">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">


                            <td>${f.getName()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${dirs.size}" />
            </div>
        </div>
    </body>
</html>
