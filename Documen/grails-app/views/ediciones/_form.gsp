<%@ page import="hola.documental.Ediciones" %>



<div class="fieldcontain ${hasErrors(bean: edicionesInstance, field: 'fechaPortada', 'error')} required">
	<label for="fechaPortada">
		<g:message code="ediciones.fechaPortada.label" default="Fecha Portada" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fechaPortada" precision="day"  value="${edicionesInstance?.fechaPortada}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: edicionesInstance, field: 'imagen', 'error')} ">
	<label for="imagen">
		<g:message code="ediciones.imagen.label" default="Imagen" />
		
	</label>
	<g:textField name="imagen" value="${edicionesInstance?.imagen}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: edicionesInstance, field: 'nombre', 'error')} ">
	<label for="nombre">
		<g:message code="ediciones.nombre.label" default="Nombre" />
		
	</label>
	<g:textField name="nombre" value="${edicionesInstance?.nombre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: edicionesInstance, field: 'numero', 'error')} required">
	<label for="numero">
		<g:message code="ediciones.numero.label" default="Numero" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="numero" type="number" value="${edicionesInstance.numero}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: edicionesInstance, field: 'publicacion', 'error')} required">
	<label for="publicacion">
		<g:message code="ediciones.publicacion.label" default="Publicacion" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="publicacion" name="publicacion.id" from="${hola.documental.Publicacion.list()}" optionKey="id" required="" value="${edicionesInstance?.publicacion?.id}" class="many-to-one"/>
</div>

