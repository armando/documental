
<%@ page import="hola.documental.Ediciones" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'ediciones.label', default: 'Ediciones')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-ediciones" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-ediciones" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="fechaPortada" title="${message(code: 'ediciones.fechaPortada.label', default: 'Fecha Portada')}" />
					
						<g:sortableColumn property="imagen" title="${message(code: 'ediciones.imagen.label', default: 'Imagen')}" />
					
						<g:sortableColumn property="nombre" title="${message(code: 'ediciones.nombre.label', default: 'Nombre')}" />
					
						<g:sortableColumn property="numero" title="${message(code: 'ediciones.numero.label', default: 'Numero')}" />
					
						<th><g:message code="ediciones.publicacion.label" default="Publicacion" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${edicionesInstanceList}" status="i" var="edicionesInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${edicionesInstance.id}">${fieldValue(bean: edicionesInstance, field: "fechaPortada")}</g:link></td>
					
						<td>${fieldValue(bean: edicionesInstance, field: "imagen")}</td>
					
						<td>${fieldValue(bean: edicionesInstance, field: "nombre")}</td>
					
						<td>${fieldValue(bean: edicionesInstance, field: "numero")}</td>
					
						<td>${fieldValue(bean: edicionesInstance, field: "publicacion")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${edicionesInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
