
<%@ page import="hola.documental.Ediciones" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<g:set var="entityName" value="${message(code: 'ediciones.label', default: 'Ediciones')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-ediciones" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-ediciones" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list ediciones">
			
				<g:if test="${edicionesInstance?.fechaPortada}">
				<li class="fieldcontain">
					<span id="fechaPortada-label" class="property-label"><g:message code="ediciones.fechaPortada.label" default="Fecha Portada" /></span>
					
						<span class="property-value" aria-labelledby="fechaPortada-label"><g:formatDate date="${edicionesInstance?.fechaPortada}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${edicionesInstance?.imagen}">
				<li class="fieldcontain">
					<span id="imagen-label" class="property-label"><g:message code="ediciones.imagen.label" default="Imagen" /></span>
					
						<span class="property-value" aria-labelledby="imagen-label"><g:fieldValue bean="${edicionesInstance}" field="imagen"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${edicionesInstance?.nombre}">
				<li class="fieldcontain">
					<span id="nombre-label" class="property-label"><g:message code="ediciones.nombre.label" default="Nombre" /></span>
					
						<span class="property-value" aria-labelledby="nombre-label"><g:fieldValue bean="${edicionesInstance}" field="nombre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${edicionesInstance?.numero}">
				<li class="fieldcontain">
					<span id="numero-label" class="property-label"><g:message code="ediciones.numero.label" default="Numero" /></span>
					
						<span class="property-value" aria-labelledby="numero-label"><g:fieldValue bean="${edicionesInstance}" field="numero"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${edicionesInstance?.publicacion}">
				<li class="fieldcontain">
					<span id="publicacion-label" class="property-label"><g:message code="ediciones.publicacion.label" default="Publicacion" /></span>
					
						<span class="property-value" aria-labelledby="publicacion-label"><g:link controller="publicacion" action="show" id="${edicionesInstance?.publicacion?.id}">${edicionesInstance?.publicacion?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:edicionesInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${edicionesInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
