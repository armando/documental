class UrlMappings {
    static mappings = {
	  "/$controller/view/$edicion/$pagina(.$format)?"(controller: "pagina", action: "view")
	  "/busqueda" (controller: "pagina", action: "busca")
	  "/busca" (controller: "pagina", action: "busca")
	  "/documentacion" (controller: "pagina", action: "hola")
      "/$controller/$action?/$id?"{
	      constraints {
			 // apply constraints here
		  }
	  }
	  "/"(controller: "pagina", action: "busca")
	  "500"(view:'/error')
	}
}
