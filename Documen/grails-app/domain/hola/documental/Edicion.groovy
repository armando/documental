package hola.documental

import java.text.DateFormat

class Edicion implements Comparable, Serializable{
    Integer numero
    String nombre
    Date fechaPortada
    Date fechaPublicacion
    Publicacion publicacion
    String repositorio
    Tipo tipo

    Date lastIndex
    Date dateCreated
    Date lastUpdated

    static hasMany= [paginas: Pagina]

    String toString(){
        return nombre?:String.format('%d : %td /%<tm /%<tY', numero, fechaPortada)
    }

    int compareTo(Object e){
        return (e instanceof Edicion) ? e.fechaPortada - fechaPortada : 0
    }
}
