package hola.documental

import java.util.Date;

class Hola {
    String personaje
    String nacion
    String profesion
    String nacimiento
    String muerte
    String revista
    String numero
    String autor
    String observaciones
    String codigo
    Date fecha

	static hasMany = [paginas: Pagina]
	
    static constraints= {
        numero()
        fecha()
        personaje()
        autor()
        observaciones()
        codigo()
        nacion(nulleable:true)
        nacimiento(nulleable:true)
        profesion(nulleable:true)
        muerte(nulleable:true)
    }
    
    static mapping = {
        autor(type: 'text')
        personaje(type: 'text')
        observaciones(type: 'text')
    }
}
