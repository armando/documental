package hola.documental

class Pagina implements Serializable {
	String codigo
    Publicacion publicacion
    Integer versionE
    boolean publicada
    Ediciones edicion
    Integer tomos
    Integer tipo
    Integer numero
    Integer versionP
	String titular
    String contenido
    String contenidoPDF
    String path
	String file
    
    Date dateCreated
    Date lastUpdated

    static mapping = {
        //id composite:['publicacion', 'edicion', 'numero']
        contenido type:'text'
        contenidoPDF type:'text'
    }

    static constraints = {
		codigo(nullable:true)
		edicion(nullable:true)
        contenido(nullable:true)
        contenidoPDF()
    }
}
