package hola.documental

import java.util.Date;

class Publicacion {
    Integer codigo
    String nombre
	String path
    Integer orden

	Date dateCreated
	Date lastUpdated
	
    static hasMany = [ediciones: Ediciones]

    static constraints = {
    }

    String toString(){
        return nombre
    }
}
