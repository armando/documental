package hola.documental

import java.util.Date;

class Ediciones implements Comparable, Serializable{
  Publicacion publicacion
  Integer numero
  String imagen
  Date fechaPortada
  String nombre
  
  Date dateCreated
  Date lastUpdated
  
    String toString(){
        return String.format('%d : %td /%<tm /%<tY', numero, fechaPortada)
    }

    int compareTo(Object e){
        return (e instanceof Ediciones) ? e.fechaPortada - fechaPortada : 0
    }
}
