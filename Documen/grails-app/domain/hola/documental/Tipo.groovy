package hola.documental

class Tipo implements Serializable, Comparable {
    Integer codigo
    Integer orden
    String nombre

    static hasMany = [ediciones: Ediciones]

    String toString(){
        return nombre
    }

    int compareTo(Object o){
        return orden.compareTo(o.orden)
    }
}
