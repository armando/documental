package hola.documental

class Archivo {
    String zona
    String carpeta
    String letra
    String conexiones
    String acontecimiento
    
    static searchable= true
    static constraints= {
        zona(nullable: true, inList:['GARAJE', 'GALERAS'])
        carpeta()
        letra()
        conexiones(nullable: true)
        acontecimiento(nullable: true)
    }
    
    static mapping = {
        conexiones(type: 'text')
        acontecimiento(type: 'text')
    }
}
