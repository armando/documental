package hola.documental

class Articulo {
    Edicion edicion
    Integer numero
    String antetitulo
    String titulo
    String subtitulo
    Autor autor
    String observaciones
    String keywords
    String texto

    Date dateCreated
    Date lastUpdated

    static hasMany = [paginas: Pagina]

    static optionals= ['antetitulo', 'subtitulo', 'observaciones', 'keywords']

    static constraints = {
        numero(max:10000)
        antetitulo(maxSize: 100, nullable: true)
        titulo(maxSize: 254)
        subtitulo(maxSize: 200, nullable: true)
        autor(nullable: true)
        observaciones(maxSize: 254, nullable:true)
        keywords(maxSize: 200, nullable:true)
        texto(nullable: true)
    }
}
