package hola.administracion

class Registro {
    String departamento
    Date fecha
    String destinatario
    String remitente
    String concepto
    Integer year
    Date nueva_fecha

    static constraints= {
        departamento()
        fecha(nullable: true)
        destinatario()
        remitente()
        concepto()
        year(nullable: true)
        nueva_fecha(nullable: true)
    }
    
    static mapping = {
        remitente(type: 'text')
        destinatario(type: 'text')
        concepto(type: 'text')
    }
}
