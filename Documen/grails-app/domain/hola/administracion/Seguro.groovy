package hola.administracion

class Seguro {
    String poliza
    Date fecha
    String vencimiento
    String clase
    String lugar
    
    static searchable= true
    static constraints= {
        poliza()
        fecha(nullable: true)
        vencimiento()
        clase()
        lugar()
    }
    
    static mapping = {
        poliza(type: 'text')
        vencimiento(type: 'text')
        clase(type: 'text')
        lugar(type: 'text')
    }
}
