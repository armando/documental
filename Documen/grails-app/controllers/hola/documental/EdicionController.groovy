package hola.documental

class EdicionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def paginaService

    def browse = {
        def dir= new File(params.dir?:"/Users/armando")
        if (params.subdir){
            dir= new File(dir, params.subdir)
        }
        def list= dir.listFiles()
        def dirs= list.findAll{it.isDirectory() && !it.isHidden()}
        def files= list.findAll{it.isFile() && !it.isHidden()}
        [dir: dir, dirs: dirs, files: files]
    }

    def parse = {
        def dir= new File(params.dir)
        if (dir){
            dir.eachFile{paginaService.parse(it)}
        }
    }
    
    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [edicionInstanceList: Edicion.list(params), edicionInstanceTotal: Edicion.count()]
    }

    def create = {
        def edicionInstance = new Edicion()
        edicionInstance.properties = params
        return [edicionInstance: edicionInstance]
    }

    def save = {
        def edicionInstance = new Edicion(params)
        if (edicionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'edicion.label', default: 'Edicion'), edicionInstance.id])}"
            redirect(action: "show", id: edicionInstance.id)
        }
        else {
            render(view: "create", model: [edicionInstance: edicionInstance])
        }
    }

    def show = {
        def edicionInstance = Edicion.get(params.id)
        if (!edicionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'edicion.label', default: 'Edicion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [edicionInstance: edicionInstance]
        }
    }

    def edit = {
        def edicionInstance = Edicion.get(params.id)
        if (!edicionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'edicion.label', default: 'Edicion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [edicionInstance: edicionInstance]
        }
    }

    def update = {
        def edicionInstance = Edicion.get(params.id)
        if (edicionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (edicionInstance.version > version) {
                    
                    edicionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'edicion.label', default: 'Edicion')] as Object[], "Another user has updated this Edicion while you were editing")
                    render(view: "edit", model: [edicionInstance: edicionInstance])
                    return
                }
            }
            edicionInstance.properties = params
            if (!edicionInstance.hasErrors() && edicionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'edicion.label', default: 'Edicion'), edicionInstance.id])}"
                redirect(action: "show", id: edicionInstance.id)
            }
            else {
                render(view: "edit", model: [edicionInstance: edicionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'edicion.label', default: 'Edicion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def edicionInstance = Edicion.get(params.id)
        if (edicionInstance) {
            try {
                edicionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'edicion.label', default: 'Edicion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'edicion.label', default: 'Edicion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'edicion.label', default: 'Edicion'), params.id])}"
            redirect(action: "list")
        }
    }
}
