package hola.documental

class PublicacionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [publicacionInstanceList: Publicacion.list(params), publicacionInstanceTotal: Publicacion.count()]
    }

    def create = {
        def publicacionInstance = new Publicacion()
        publicacionInstance.properties = params
        return [publicacionInstance: publicacionInstance]
    }

    def save = {
        def publicacionInstance = new Publicacion(params)
        if (publicacionInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'publicacion.label', default: 'Publicacion'), publicacionInstance.id])}"
            redirect(action: "show", id: publicacionInstance.id)
        }
        else {
            render(view: "create", model: [publicacionInstance: publicacionInstance])
        }
    }

    def show = {
        def publicacionInstance = Publicacion.get(params.id)
        if (!publicacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'publicacion.label', default: 'Publicacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            [publicacionInstance: publicacionInstance]
        }
    }

    def edit = {
        def publicacionInstance = Publicacion.get(params.id)
        if (!publicacionInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'publicacion.label', default: 'Publicacion'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [publicacionInstance: publicacionInstance]
        }
    }

    def update = {
        def publicacionInstance = Publicacion.get(params.id)
        if (publicacionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (publicacionInstance.version > version) {
                    
                    publicacionInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'publicacion.label', default: 'Publicacion')] as Object[], "Another user has updated this Publicacion while you were editing")
                    render(view: "edit", model: [publicacionInstance: publicacionInstance])
                    return
                }
            }
            publicacionInstance.properties = params
            if (!publicacionInstance.hasErrors() && publicacionInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'publicacion.label', default: 'Publicacion'), publicacionInstance.id])}"
                redirect(action: "show", id: publicacionInstance.id)
            }
            else {
                render(view: "edit", model: [publicacionInstance: publicacionInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'publicacion.label', default: 'Publicacion'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def publicacionInstance = Publicacion.get(params.id)
        if (publicacionInstance) {
            try {
                publicacionInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'publicacion.label', default: 'Publicacion'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'publicacion.label', default: 'Publicacion'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'publicacion.label', default: 'Publicacion'), params.id])}"
            redirect(action: "list")
        }
    }
}
