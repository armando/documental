package hola.documental

import hola.documental.Archivo;

class ArchivoController {
    def elasticSearchService, elasticSearchAdminService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }
    
    def imp= {
        def DBTWRecordSet = new XmlSlurper().parse(new File("${params.file}"))
        def archivo, carpeta, conexiones, acontecimiento
        DBTWRecordSet.DBTWRecord.each() {r ->
            carpeta= r.CARPETA?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            conexiones= r.CONEXIONES?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            acontecimiento= r.ACONTECIMIENTO?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            archivo= new Archivo(zona:params.zona, carpeta: carpeta, letra: r.LETRA.text(), conexiones: conexiones, acontecimiento: acontecimiento)
            if (archivo.validate()){
                archivo.save()
            }
            else{
                println carpeta
                archivo.errors.allErrors.each {
                    println it
                }
            }
        }
    }

    def reset = {
        elasticSearchAdminService.deleteIndex()
    }
     
    def search= {
        def res
        if (params.query || params.carpeta || params.conexiones || params.acontecimiento){            
            def c= Archivo.createCriteria()
            if (params.query){
                res = c {
                    or {
                        ilike("carpeta", "%${params.query}%")
                        ilike("conexiones", "%${params.query}%")
                        ilike("acontecimiento", "%${params.query}%")
                    }
                }
            }
            else{
                res = c {
                    and {
                        ilike("carpeta", "%${params.carpeta}%")
                        ilike("conexiones", "%${params.conexiones}%")
                        ilike("acontecimiento", "%${params.acontecimiento}%")
                    }
                }
            }
        }
        params.max = Math.min(params.max ? params.int('max') : 25, 100)
        def subres=(params.zona=="TODO") ? res : res?.findAll{r -> r.zona==params.zona}
        [params:params, query: params.query, zona: params.zona, archivoInstanceList: subres , archivoInstanceTotal: subres?subres.size():0]
    }
   
    def search_elastic= {
        def res
        if (params.query){            
            res= Archivo.search(searchType:'dfs_query_and_fetch'){
                bool {
                    must {
                        query_string(query: params.query, default_operator: 'AND', default_field: 'carpeta')
                    }
                }
            }
        }
        params.max = Math.min(params.max ? params.int('max') : 25, 100)
        def subres=(params.zona=="TODO") ? res?.searchResults : res?.searchResults.findAll{r -> r.zona==params.zona}
        [params:params, query: params.query, zona: params.zona, archivoInstanceList: subres , archivoInstanceTotal: subres?subres.size():0]        
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 25, 100)
        [archivoInstanceList: Archivo.list(params), archivoInstanceTotal: Archivo.count()]
    }

    def create = {
        def archivoInstance = new Archivo()
        archivoInstance.properties = params
        return [archivoInstance: archivoInstance]
    }

    def save = {
        def archivoInstance = new Archivo(params)
        archivoInstance.carpeta= archivoInstance.carpeta.replaceAll('\\.', '. ').replaceAll('  ', ' ')
        archivoInstance.conexiones= archivoInstance.carpeta.replaceAll('\\.', '. ').replaceAll('  ', ' ')
        archivoInstance.acontecimiento= archivoInstance.carpeta.replaceAll('\\.', '. ').replaceAll('  ', ' ')
        if (archivoInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'archivo.label', default: 'Archivo'), archivoInstance.id])}"
            redirect(action: "show", id: archivoInstance.id)
        }
        else {
            render(view: "create", model: [archivoInstance: archivoInstance])
        }
    }

    def show = {
        def archivoInstance = Archivo.get(params.id)
        if (!archivoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'archivo.label', default: 'Archivo'), params.id])}"
            redirect(action: "list")
        }
        else {
            [archivoInstance: archivoInstance]
        }
    }

    def edit = {
        def archivoInstance = Archivo.get(params.id)
        if (!archivoInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'archivo.label', default: 'Archivo'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [archivoInstance: archivoInstance]
        }
    }

    def update = {
        def archivoInstance = Archivo.get(params.id)
        if (archivoInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (archivoInstance.version > version) {
                    
                    archivoInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'archivo.label', default: 'Archivo')] as Object[], "Another user has updated this Archivo while you were editing")
                    render(view: "edit", model: [archivoInstance: archivoInstance])
                    return
                }
            }
            archivoInstance.properties = params
            if (!archivoInstance.hasErrors() && archivoInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'archivo.label', default: 'Archivo'), archivoInstance.id])}"
                redirect(action: "show", id: archivoInstance.id)
            }
            else {
                render(view: "edit", model: [archivoInstance: archivoInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'archivo.label', default: 'Archivo'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def archivoInstance = Archivo.get(params.id)
        if (archivoInstance) {
            try {
                archivoInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'archivo.label', default: 'Archivo'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'archivo.label', default: 'Archivo'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'archivo.label', default: 'Archivo'), params.id])}"
            redirect(action: "list")
        }
    }
}
