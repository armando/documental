package hola.documental

import org.springframework.dao.DataIntegrityViolationException

class EdicionesController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [edicionesInstanceList: Ediciones.list(params), edicionesInstanceTotal: Ediciones.count()]
    }

    def create() {
        [edicionesInstance: new Ediciones(params)]
    }

    def save() {
        def edicionesInstance = new Ediciones(params)
        if (!edicionesInstance.save(flush: true)) {
            render(view: "create", model: [edicionesInstance: edicionesInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'ediciones.label', default: 'Ediciones'), edicionesInstance.id])
        redirect(action: "show", id: edicionesInstance.id)
    }

    def show(Long id) {
        def edicionesInstance = Ediciones.get(id)
        if (!edicionesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'ediciones.label', default: 'Ediciones'), id])
            redirect(action: "list")
            return
        }

        [edicionesInstance: edicionesInstance]
    }

    def edit(Long id) {
        def edicionesInstance = Ediciones.get(id)
        if (!edicionesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'ediciones.label', default: 'Ediciones'), id])
            redirect(action: "list")
            return
        }

        [edicionesInstance: edicionesInstance]
    }

    def update(Long id, Long version) {
        def edicionesInstance = Ediciones.get(id)
        if (!edicionesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'ediciones.label', default: 'Ediciones'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (edicionesInstance.version > version) {
                edicionesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'ediciones.label', default: 'Ediciones')] as Object[],
                          "Another user has updated this Ediciones while you were editing")
                render(view: "edit", model: [edicionesInstance: edicionesInstance])
                return
            }
        }

        edicionesInstance.properties = params

        if (!edicionesInstance.save(flush: true)) {
            render(view: "edit", model: [edicionesInstance: edicionesInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'ediciones.label', default: 'Ediciones'), edicionesInstance.id])
        redirect(action: "show", id: edicionesInstance.id)
    }

    def delete(Long id) {
        def edicionesInstance = Ediciones.get(id)
        if (!edicionesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'ediciones.label', default: 'Ediciones'), id])
            redirect(action: "list")
            return
        }

        try {
            edicionesInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'ediciones.label', default: 'Ediciones'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'ediciones.label', default: 'Ediciones'), id])
            redirect(action: "show", id: id)
        }
    }
}
