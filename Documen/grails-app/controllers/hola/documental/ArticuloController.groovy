package hola.documental

class ArticuloController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    def static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        if(!params.max) params.max = 10
        [ articuloList: Articulo.list( params ) ]
    }

    def show = {
        def articulo = Articulo.get( params.id )

        if(!articulo) {
            flash.message = "Articulo not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ articulo : articulo ] }
    }

    def delete = {
        def articulo = Articulo.get( params.id )
        if(articulo) {
            articulo.delete()
            flash.message = "Articulo ${params.id} deleted"
            redirect(action:list)
        }
        else {
            flash.message = "Articulo not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def articulo = Articulo.get( params.id )

        if(!articulo) {
            flash.message = "Articulo not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ articulo : articulo ]
        }
    }

    def update = {
        def articulo = Articulo.get( params.id )
        if(articulo) {
            articulo.properties = params
            if(!articulo.hasErrors() && articulo.save()) {
                flash.message = "Articulo ${params.id} updated"
                redirect(action:show,id:articulo.id)
            }
            else {
                render(view:'edit',model:[articulo:articulo])
            }
        }
        else {
            flash.message = "Articulo not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def articulo = new Articulo()
        articulo.properties = params
        return ['articulo':articulo]
    }

    def save = {
        def articulo = new Articulo(params)
        if(!articulo.hasErrors() && articulo.save()) {
            flash.message = "Articulo ${articulo.id} created"
            redirect(action:show,id:articulo.id)
        }
        else {
            render(view:'create',model:[articulo:articulo])
        }
    }
}
