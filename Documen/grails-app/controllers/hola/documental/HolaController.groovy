package hola.documental

import hola.documental.Hola
import java.util.regex.Matcher
import java.util.regex.Pattern
import grails.converters.JSON
import org.apache.solr.common.SolrInputDocument
import org.springframework.dao.DataIntegrityViolationException
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.impl.HttpSolrServer
import org.apache.solr.client.solrj.response.QueryResponse


class HolaController {
    //def elasticSearchService, elasticSearchAdminService
	def paginaService, textService
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "busca", params: params)
    }
    
    def imp= {
        def DBTWRecordSet = new XmlSlurper().parse(new File("${params.file}"))
        def hola, personaje, revista, numero, autor, observaciones, codigo, fecha, nfecha, nacion, profesion, nacimiento, muerte, bodas, hijos
        DBTWRecordSet.DBTWRecord.each() {r ->
            personaje= r.PERSONAJE?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            revista= r.REVISTA?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            numero= r.NUMERO?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            autor= r.AUTOR?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            nacion= r.NACION?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            profesion= r.PROFESION?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            nacimiento= r.NACIMIENTO?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            bodas= r.BODAS?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            hijos= r.HIJOS?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            muerte= r.MUERTE?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            observaciones= r.OBSERVACIONES?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            codigo= r.CODIGO?.text().replaceAll('\\.', '. ').replaceAll('  ', ' ')
            fecha= r.FECHA?.text()

            try{
                nfecha=  new Date().parse("dd-MM-yyyy", fecha)
            }
            catch (Exception e){
                try{
                    nfecha=  new Date().parse("ddMMyyyy", fecha)
                }
                catch (Exception e1){
                    try{
                        nfecha=  new Date().parse("dd.MM.yyyy", fecha)
                    }
                    catch (Exception e2){
                        try{
                            nfecha=  new Date().parse("dd/MM/yyyy", fecha)
                        }
                        catch (Exception e3){
                            try{
                                nfecha=  new Date().parse("dd-MM-yy", fecha)
                            }
                            catch (Exception e4){
                                try{
                                    nfecha=  new Date().parse("dd/MM/yy", fecha)
                                }
                                catch (Exception e5){
                                    try{
                                        nfecha=  new Date().parse("dd-MM-yy", fecha)
                                    }
                                    catch (Exception e6){
                                        try{
                                            nfecha=  new Date().parse("dd.MM-yyyy", fecha)
                                        }
                                        catch (Exception e7){
                                            try{
                                                nfecha=  new Date().parse("d de MMMM de yyyy", fecha)
                                            }
                                            catch (Exception e8){
                                                if (fecha!=""){
                                                    println "Error nfecha (-${fecha}-) en ${personaje}"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            hola= new Hola(personaje:personaje, revista:revista, numero:numero, autor:autor, observaciones:observaciones, codigo:codigo, fecha: nfecha, nacion:nacion, profesion:profesion, nacimiento: nacimiento, muerte: muerte, bodas: bodas, hijos: hijos)
            if (hola.validate()){
                hola.save()
            }
            else{
                println personaje
                hola.errors.allErrors.each {
                    println it
                }
            }
        }
    }
	
	def analyze() {
		def reg= Hola.get(353608)
		def list= reg.numero.tokenize('\n')
		def m,p,e 
		for (ref in list) {
			m= ref =~ /([0-9]+)[^0-9]*([0-9]+)?/
			if (m) {
				e= Ediciones.findByNumero(m[0][1])
				p= Pagina.findByEdicionAndNumero(e,m[0][2]?:1)
				if (p) {
				reg.paginas.add(p)
				}
				else {
					log.error(m[0])
				}
			}
		}
		reg.save()
	}
    
	def busca() {
		def results, facetResults

		log.debug(params)
		String url = "http://localhost:8983/solr/documentacion"
		def server = new HttpSolrServer(url)

		params.max= params.max?:"10"
		params.offset= params.offset?:"0"
		//def start= params.start ? params.start.format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", GMT) : "*"
		//def end= params.end ? params.end.format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", GMT) : "*"
		//def range= "fecha:["+start+" TO "+end+"]"
		//log.debug(range)
		def range= "edicion:["+(params.start?:"*")+" TO "+(params.end?:"*")+"]"

		def q= (params.q) ? params.q : "*:*"

		def solrQuery =
				new SolrQuery().
				setQuery(q).set("cache","false").
				//set("defType","edismax").
				setStart(params.offset.toInteger()).setRows(params.max.toInteger()).
				//set("pf","text").
				//addFilterQuery(params.publicacion ? "publicacion:"+params.publicacion : null).
				addFilterQuery(params.edicion ? "numero:"+params.edicion : null)

		//solrQuery2= params.sort ? solrQuery.addSort(params.sort, SolrQuery.ORDER.valueOf(params.order)) : solrQuery

		QueryResponse rsp = server.query(solrQuery)
		results= rsp.getResults()
		facetResults= rsp.getFacetFields()


		//log.debug(rsp)
		[params: params, documentoList: results, facetResults: facetResults, documentoTotal: results?.getNumFound()]
	}	
	
    def search= {
        def results
        if (params.query){            
            def c= Hola.createCriteria()
            results = c {
                or {
                    ilike("personaje", "%${params.query}%")
                    ilike("numero", "%${params.query}%")
                    ilike("autor", "%${params.query}%")
                    ilike("observaciones", "%${params.query}%")
                }
            }
        }
        params.max = Math.min(params.max ? params.int('max') : 25, 100)
        [params:params, query: params.query, zona: params.zona, holaInstanceList: results , holaInstanceTotal: results?results.size():0]        
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 25, 100)
        [holaInstanceList: Hola.list(params), holaInstanceTotal: Hola.count()]
    }

    def create = {
        def holaInstance = new Hola()
        holaInstance.properties = params
        return [holaInstance: holaInstance]
    }

    def save = {
        def holaInstance = new Hola(params)
        holaInstance.carpeta= holaInstance.carpeta.replaceAll('\\.', '. ').replaceAll('  ', ' ')
        holaInstance.conexiones= holaInstance.carpeta.replaceAll('\\.', '. ').replaceAll('  ', ' ')
        holaInstance.acontecimiento= holaInstance.carpeta.replaceAll('\\.', '. ').replaceAll('  ', ' ')
        if (holaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'hola.label', default: 'Hola'), holaInstance.id])}"
            redirect(action: "show", id: holaInstance.id)
        }
        else {
            render(view: "create", model: [holaInstance: holaInstance])
        }
    }

    def show = {
        def holaInstance = Hola.get(params.id)
        if (!holaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'hola.label', default: 'Hola'), params.id])}"
            redirect(action: "list")
        }
        else {
            [holaInstance: holaInstance]
        }
    }

    def edit = {
        def holaInstance = Hola.get(params.id)
        if (!holaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'hola.label', default: 'Hola'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [holaInstance: holaInstance]
        }
    }

    def update = {
        def holaInstance = Hola.get(params.id)
        if (holaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (holaInstance.version > version) {
                    
                    holaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'hola.label', default: 'Hola')] as Object[], "Another user has updated this Hola while you were editing")
                    render(view: "edit", model: [holaInstance: holaInstance])
                    return
                }
            }
            holaInstance.properties = params
            if (!holaInstance.hasErrors() && holaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'hola.label', default: 'Hola'), holaInstance.id])}"
                redirect(action: "show", id: holaInstance.id)
            }
            else {
                render(view: "edit", model: [holaInstance: holaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'hola.label', default: 'Hola'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def holaInstance = Hola.get(params.id)
        if (holaInstance) {
            try {
                holaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'hola.label', default: 'Hola'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'hola.label', default: 'Hola'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'hola.label', default: 'Hola'), params.id])}"
            redirect(action: "list")
        }
    }
}
