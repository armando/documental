package hola.documental

import grails.converters.JSON
import org.apache.solr.common.SolrInputDocument
import org.springframework.dao.DataIntegrityViolationException
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.impl.HttpSolrServer
import org.apache.solr.client.solrj.response.QueryResponse


class PaginaController {
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	static GMT= TimeZone.getTimeZone("GMT")

	def paginaService, textService

	def imp() {
		def completa= false

		if (params.dir) {
			def dir= new File(params.dir)
			def replace= new Boolean(params.replace)
			def recurse= new Boolean(params.recurse)
			def inicio= params.inicio as Integer
			def fin= params.fin ? params.fin as Integer : inicio

			if (recurse){
				parseAll(dir, replace)
			}
			else{
				(inicio..fin).each {
					def subdir= new File(dir, it.toString())
					if (subdir.exists()) {
						log.debug("Entrando en ${subdir}")
						subdir.eachFile{
							if (it.name.endsWith("pdf")) {
								paginaService.parse(it, replace)
							}
							else {
								log.debug("No pdf: ${it.absolutePath}")
							}
						}
					}
				}
			}
			completa= true
		}
		[params: params, completa: completa]
	}

	def comprobar() {
		def inicio= params.inicio ? params.inicio as Integer : 0
		def fin= params.fin ? params.fin as Integer : inicio
		def faltan= [:]
		def ed, pag, prev

		log.debug("Inicio ${inicio} Fin ${fin}")
		File f= new File("/tmp/falta.txt")

		(inicio..fin).each {
			ed= Ediciones.findByNumero(it)
			log.debug("Edicion ${ed}")
			
			def max= Pagina.createCriteria().get {
				eq('edicion', ed)
				projections {
					max "numero"
				}
			} as Long
			
			log.debug(max)						
			if (max) {
				faltan[ed.numero]=[]
				(1..max).each {
					pag= Pagina.findByEdicionAndNumero(ed, it)
					if (!pag) {
						prev= Pagina.findByEdicionAndNumero(ed, it-1)
						if (prev?.tipo!=2) {
							faltan[ed.numero].add(it)
						}
					}
				}
			}
			else {
				faltan[ed.numero]=['todas']
			}
			if (faltan[ed.numero].size()>0) {
				f << "${ed.numero}: ${faltan[ed.numero]}\n"
			}
		}

		[params: params, faltan: faltan]
	}


	private parseAll(File f, boolean replace) {
		if (f.isFile()) {
			if (f.name.endsWith("pdf")) {
				paginaService.parse(f, replace)
			}
			else if (!f.isHidden()) {
				log.warn("No pdf: ${f.absolutePath}")
			}
		}
		else {
			f.eachFile{ parseAll(it, replace) }
		}
	}

	def search() {
		log.debug(params)
		String url = "http://localhost:8983/solr/collection1"
		def server = new HttpSolrServer( url )

		params.max= params.max?:"10"
		params.offset= params.offset?:"0"
		//def start= params.start ? params.start.format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", GMT) : "*"
		//def end= params.end ? params.end.format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", GMT) : "*"
		//def range= "fecha:["+start+" TO "+end+"]"
		//log.debug(range)
		def range= "edicion:["+params.start+" TO "+params.end+"]"


		def q= (params.q) ? params.q : "*:*"

		def solrQuery =
				new SolrQuery().
				setQuery(q).set("cache","false").
				set("defType","edismax").
				set("op","AND").
				setStart(params.offset.toInteger()).setRows(params.max.toInteger()).
				set("pf","text").
				//set("tv.fl","contenido").set("tv",true).set("tv.positions",true).
				//setFields("publicacion","edicion^10","contenido").
				//setFacet(true).setFacetMinCount(1).setFacetLimit(8).
				//addFacetField("publicacion").addFacetField("edicion").
				addFilterQuery(params.publicacion ? "publicacion:"+params.publicacion : "").
				addFilterQuery(params.edicion ? "edicion:"+params.edicion : "").
				addFilterQuery(params.numero ? "numero:"+params.numero : "").
				addFilterQuery(range)

		QueryResponse rsp = server.query(solrQuery)
		def results= rsp.getResults()
		def facetResults= rsp.getFacetFields()
		//log.debug(rsp)
		[params: params, paginaList: results, facetResults: facetResults, paginaTotal: results.getNumFound()]
	}

	def busca() {
		def results, facetResults

		log.debug(params)
		String url = "http://localhost:8983/solr/collection1"
		def server = new HttpSolrServer(url)

		params.max= params.max?:"10"
		params.offset= params.offset?:"0"
		//def start= params.start ? params.start.format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", GMT) : "*"
		//def end= params.end ? params.end.format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", GMT) : "*"
		//def range= "fecha:["+start+" TO "+end+"]"
		//log.debug(range)
		def range= "edicion:["+(params.start?:"*")+" TO "+(params.end?:"*")+"]"

		def q= (params.q) ? params.q : "*:*"

		def solrQuery =
				new SolrQuery().
				setQuery(q).set("cache","false").
				set("op", "AND").
				set("defType","edismax").
				setStart(params.offset.toInteger()).setRows(params.max.toInteger()).
				//set("pf","text").
				//addFilterQuery(params.publicacion ? "publicacion:"+params.publicacion : null).
				addFilterQuery(params.edicion ? "edicion:"+params.edicion : null).
				addFilterQuery(params.pagina ? "numero:"+params.pagina : null).
				addFilterQuery(params.edicion ? null : range)
		solrQuery= params.sort ? solrQuery.addSort(params.sort, SolrQuery.ORDER.valueOf(params.order)) : solrQuery

		QueryResponse rsp = server.query(solrQuery)
		results= rsp.getResults()
		facetResults= rsp.getFacetFields()

		//log.debug(rsp)
		[params: params, paginaList: results, facetResults: facetResults, paginaTotal: results?.getNumFound()]
	}

	def range() {
		def publicacion = Publicacion.get(params.publicacion)
		//log.debug(params)
		def ediciones= Ediciones.findAllByPublicacionAndFechaPortadaLessThan(publicacion,new Date()+7, [sort:'fechaPortada', order:'asc'])

		//log.debug(ediciones.size())
		render(template: 'range', model:  [ediciones: ediciones])
	}

	def row(Integer ed, Integer pag, String q, Integer context) {
		log.debug(params)
		def pagctx, pags=[]
		def edicion= Ediciones.findByNumero(ed)
		def pagina= Pagina.findByEdicionAndNumero(edicion, pag)
		if (!pagina) {
			log.error("No existe la página ${pag} de la edicion ${num}")
			return
		}
		def hl= textService.highlight(q, pagina.contenido, 50)
		for (i in -context..context) {
			if (pag+(i*2)>0) {
				pagctx= Pagina.findByEdicionAndNumero(edicion,pag+(i*2))
				if (pagctx) {
					pags.add(pagctx)
				}
			}
		}
		render(template: 'row', model:  [pagina: pagina, ed: ed, q:params.q, row:params.row, hl:hl, context: context, pags: pags])
	}

	def pdf(Integer edicion, String rango) {
		log.debug(params)
		def ed= Ediciones.findByNumero(edicion)
		def defaultfin= Pagina.findAllByEdicion(ed, [sort: 'numero', order: 'desc'])
		if (defaultfin) {
			def m = rango =~ /([0-9]*)([-\/\s])?([0-9]*)?/
			log.debug(m[0])
			def inicio= m && m[0][1].isInteger() ? m[0][1] as Integer : 1
			def fin= m && m[0][3].isInteger() ? m[0][3] as Integer : (!m || m[0][2]=='-' || m[0][2]=="/") ? defaultfin.head().numero : inicio
			log.debug("Generando PDF para ${edicion} desde ${inicio} hasta ${fin}")

			def parent= "/var/cache/pdf/${edicion}"
			def base= paginaService.filename(edicion, inicio, fin, 0, "pdf")

			def file= new File(parent, base)
			//			response.contentType = 'application/pdf'
			//			response.setHeader('Content-disposition', "inline; filename=${edicion}[${inicio}-${fin}].pdf")
			if (!file.exists()) {
				paginaService.cachepdf(ed, inicio, fin)
			}
			render(contentType: "application/pdf", file: file)

		}
		else {
			[edicion: edicion]
		}
	}


	def view() {
		def type= params.format == "pdf" ? "application" : "image"
		def ct= params.format ?: "jpg"
		def m= params.pagina =~ /([^_]*)_?([0-9]+)?/
		def pagina= m[0][1].toInteger()
		Integer width= m[0][2]? m[0][2].toInteger(): null
		def cache="/var/cache/pdf"

		def pag= Pagina.get(pagina)

		def num= pag.numero
		def ed= pag.edicion

		log.debug(num)

		def p1= (num % 2 == 0) ? num : num-1
		def p2= (num % 2 == 0) ? num+1 : num

		def file= new File("${cache}/${ed.numero}", paginaService.filename(ed.numero, p1, p2, width, params.format))

		if (!file.exists()) {
			paginaService.cacheimgs(ed, pag, type, ct, width)
		}
		render(contentType: "${type}/${ct}", file: file)
	}


	def index() {
		redirect(action: "busca")
	}

	def list(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		[paginaInstanceList: Pagina.list(params), paginaInstanceTotal: Pagina.count()]
	}

	def create() {
		[paginaInstance: new Pagina(params)]
	}

	def save() {
		def paginaInstance = new Pagina(params)
		if (!paginaInstance.save(flush: true)) {
			render(view: "create", model: [paginaInstance: paginaInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [
			message(code: 'pagina.label', default: 'Pagina'),
			paginaInstance.id
		])
		redirect(action: "show", id: paginaInstance.id)
	}

	def show(Long id) {
		def paginaInstance = Pagina.get(id)
		if (!paginaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'pagina.label', default: 'Pagina'),
				id
			])
			redirect(action: "list")
			return
		}

		[paginaInstance: paginaInstance]
	}

	def edit(Long id) {
		def paginaInstance = Pagina.get(id)
		if (!paginaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'pagina.label', default: 'Pagina'),
				id
			])
			redirect(action: "list")
			return
		}

		[paginaInstance: paginaInstance]
	}

	def update(Long id, Long version) {
		def paginaInstance = Pagina.get(id)
		if (!paginaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'pagina.label', default: 'Pagina'),
				id
			])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (paginaInstance.version > version) {
				paginaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						[
							message(code: 'pagina.label', default: 'Pagina')] as Object[],
						"Another user has updated this Pagina while you were editing")
				render(view: "edit", model: [paginaInstance: paginaInstance])
				return
			}
		}

		paginaInstance.properties = params

		if (!paginaInstance.save(flush: true)) {
			render(view: "edit", model: [paginaInstance: paginaInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [
			message(code: 'pagina.label', default: 'Pagina'),
			paginaInstance.id
		])
		redirect(action: "show", id: paginaInstance.id)
	}

	def delete(Long id) {
		def paginaInstance = Pagina.get(id)
		if (!paginaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'pagina.label', default: 'Pagina'),
				id
			])
			redirect(action: "list")
			return
		}

		try {
			paginaInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [
				message(code: 'pagina.label', default: 'Pagina'),
				id
			])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [
				message(code: 'pagina.label', default: 'Pagina'),
				id
			])
			redirect(action: "show", id: id)
		}
	}
}
