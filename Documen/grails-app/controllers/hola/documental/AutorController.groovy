package hola.documental

class AutorController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [autorInstanceList: Autor.list(params), autorInstanceTotal: Autor.count()]
    }

    def create = {
        def autorInstance = new Autor()
        autorInstance.properties = params
        return [autorInstance: autorInstance]
    }

    def save = {
        def autorInstance = new Autor(params)
        if (autorInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'autor.label', default: 'Autor'), autorInstance.id])}"
            redirect(action: "show", id: autorInstance.id)
        }
        else {
            render(view: "create", model: [autorInstance: autorInstance])
        }
    }

    def show = {
        def autorInstance = Autor.get(params.id)
        if (!autorInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'autor.label', default: 'Autor'), params.id])}"
            redirect(action: "list")
        }
        else {
            [autorInstance: autorInstance]
        }
    }

    def edit = {
        def autorInstance = Autor.get(params.id)
        if (!autorInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'autor.label', default: 'Autor'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [autorInstance: autorInstance]
        }
    }

    def update = {
        def autorInstance = Autor.get(params.id)
        if (autorInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (autorInstance.version > version) {
                    
                    autorInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'autor.label', default: 'Autor')] as Object[], "Another user has updated this Autor while you were editing")
                    render(view: "edit", model: [autorInstance: autorInstance])
                    return
                }
            }
            autorInstance.properties = params
            if (!autorInstance.hasErrors() && autorInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'autor.label', default: 'Autor'), autorInstance.id])}"
                redirect(action: "show", id: autorInstance.id)
            }
            else {
                render(view: "edit", model: [autorInstance: autorInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'autor.label', default: 'Autor'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def autorInstance = Autor.get(params.id)
        if (autorInstance) {
            try {
                autorInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'autor.label', default: 'Autor'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'autor.label', default: 'Autor'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'autor.label', default: 'Autor'), params.id])}"
            redirect(action: "list")
        }
    }
}
