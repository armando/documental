package hola.administracion

class RegistroController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def imp= {
        def DBTWRecordSet = new XmlSlurper().parse(new File("${params.file}"))
        def registro, departamento, fecha, destinatario, remitente, concepto, year, nueva_fecha, nfecha, nnueva_fecha, nyear
        DBTWRecordSet.DBTWRecord.each() {r ->
            departamento= r.DEPARTAMENTO?.text()
            fecha= r.FECHA?.text()

            try{
                nfecha=  new Date().parse("dd-MM-yyyy", fecha)
            }
            catch (Exception e){
                try{
                    nfecha=  new Date().parse("ddMMyyyy", fecha)
                }
                catch (Exception e1){
                    try{
                        nfecha=  new Date().parse("dd.MM.yyyy", fecha)
                    }
                    catch (Exception e2){
                        try{
                            nfecha=  new Date().parse("dd/MM/yyyy", fecha)
                        }
                        catch (Exception e3){
                            try{
                                nfecha=  new Date().parse("dd-MM-yy", fecha)
                            }
                            catch (Exception e4){
                                try{
                                    nfecha=  new Date().parse("dd/MM/yy", fecha)
                                }
                                catch (Exception e5){
                                    try{
                                        nfecha=  new Date().parse("dd-MM-yy", fecha)
                                    }
                                    catch (Exception e6){
                                        try{
                                            nfecha=  new Date().parse("dd.MM-yyyy", fecha)
                                        }
                                        catch (Exception e7){
                                            try{
                                                nfecha=  new Date().parse("d de MMMM de yyyy", fecha)
                                            }
                                            catch (Exception e8){
                                                if (fecha!=""){
                                                    println "Error nfecha (-${fecha}-) en ${destinatario}"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            destinatario= r.DESTINATARIO?.text()
            remitente= r.REMITENTE?.text()
            concepto= r.CONCEPTO?.text()
            year= r.YEAR?.text()
            nueva_fecha= r.NUEVAFECHA?.text()
            
            try{
                nnueva_fecha=  new Date().parse("dd/MM/yyyy", nueva_fecha)
            }
            catch (Exception e){
                if (nueva_fecha!=""){
                    println "Error nuevafecha (${nueva_fecha}) en ${destinatario} ${fecha}"
                }
                nnueva_fecha= null
            }
                        
            try{
                nyear= Integer.parseInt(year)
            }
            catch (Exception e){
                if (year!=""){
                    println "Error parseInt (${year}) en ${destinatario}"
                }
                nyear= null
            }

            registro= new Registro(departamento: departamento, fecha: nfecha , destinatario: destinatario, remitente: remitente,
                concepto: concepto, year: nyear, nueva_fecha: nnueva_fecha)
            if (registro.validate()){
                registro.save()
            }
            else{
                registro.errors.allErrors.each {
                    println it
                }
            }
        }
    }

    def search= {
        def results
        if (params.query){            
            def c= Registro.createCriteria()
            results = c {
                or {
                    ilike("remitente", "%${params.query}%")
                    ilike("concepto", "%${params.query}%")
                    ilike("destinatario", "%${params.query}%")
                }
            }
        }

        params.max = Math.min(params.max ? params.int('max') : 25, 100)
        [params:params, query: params.query, registroInstanceList: results , registroInstanceTotal: results?results.size():0]        
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [registroInstanceList: Registro.list(params), registroInstanceTotal: Registro.count()]
    }

    def create = {
        def registroInstance = new Registro()
        registroInstance.properties = params
        return [registroInstance: registroInstance]
    }

    def save = {
        def registroInstance = new Registro(params)
        if (registroInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'registro.label', default: 'Registro'), registroInstance.id])}"
            redirect(action: "show", id: registroInstance.id)
        }
        else {
            render(view: "create", model: [registroInstance: registroInstance])
        }
    }

    def show = {
        def registroInstance = Registro.get(params.id)
        if (!registroInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'registro.label', default: 'Registro'), params.id])}"
            redirect(action: "list")
        }
        else {
            [registroInstance: registroInstance]
        }
    }

    def edit = {
        def registroInstance = Registro.get(params.id)
        if (!registroInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'registro.label', default: 'Registro'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [registroInstance: registroInstance]
        }
    }

    def update = {
        def registroInstance = Registro.get(params.id)
        if (registroInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (registroInstance.version > version) {
                    
                    registroInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'registro.label', default: 'Registro')] as Object[], "Another user has updated this Registro while you were editing")
                    render(view: "edit", model: [registroInstance: registroInstance])
                    return
                }
            }
            registroInstance.properties = params
            if (!registroInstance.hasErrors() && registroInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'registro.label', default: 'Registro'), registroInstance.id])}"
                redirect(action: "show", id: registroInstance.id)
            }
            else {
                render(view: "edit", model: [registroInstance: registroInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'registro.label', default: 'Registro'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def registroInstance = Registro.get(params.id)
        if (registroInstance) {
            try {
                registroInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'registro.label', default: 'Registro'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'registro.label', default: 'Registro'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'registro.label', default: 'Registro'), params.id])}"
            redirect(action: "list")
        }
    }
}
