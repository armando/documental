package hola.administracion

class SeguroController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def imp= {
        def DBTWRecordSet = new XmlSlurper().parse(new File("${params.file}"))
        def seguro, poliza, fecha, vencimiento, clase, lugar, nfecha
        DBTWRecordSet.DBTWRecord.each() {r ->
            poliza= r.POLIZA?.text()
            fecha= r.FECHA?.text()

            try{
                nfecha=  new Date().parse("dd-MM-yyyy", fecha)
            }
            catch (Exception e){
                try{
                    nfecha=  new Date().parse("ddMMyyyy", fecha)
                }
                catch (Exception e1){
                    try{
                        nfecha=  new Date().parse("dd.MM.yyyy", fecha)
                    }
                    catch (Exception e2){
                        try{
                            nfecha=  new Date().parse("dd/MM/yyyy", fecha)
                        }
                        catch (Exception e3){
                            try{
                                nfecha=  new Date().parse("dd-MM-yy", fecha)
                            }
                            catch (Exception e4){
                                try{
                                    nfecha=  new Date().parse("dd/MM/yy", fecha)
                                }
                                catch (Exception e5){
                                    try{
                                        nfecha=  new Date().parse("dd-MM-yy", fecha)
                                    }
                                    catch (Exception e6){
                                        try{
                                            nfecha=  new Date().parse("dd.MM-yyyy", fecha)
                                        }
                                        catch (Exception e7){
                                            try{
                                                nfecha=  new Date().parse("d de MMMM de yyyy", fecha)
                                            }
                                            catch (Exception e8){
                                                if (fecha!=""){
                                                    println "Error nfecha (-${fecha}-) en ${poliza}"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            vencimiento= r.VENCIMIENTO?.text()
            clase= r.CLASE?.text()
            lugar= r.LUGAR?.text()
            
            seguro= new Seguro(poliza: poliza, fecha: nfecha, vencimiento: vencimiento, clase: clase, lugar: lugar)
            if (seguro.validate()){
                seguro.save()
            }
            else{
                seguro.errors.allErrors.each {
                    println it
                }
            }
        }
    }
    
    def search= {
        def results
        if (params.query){            
            def c= Seguro.createCriteria()
            results = c {
                or {
                    ilike("poliza", "%${params.query}%")
                    ilike("vencimiento", "%${params.query}%")
                    ilike("clase", "%${params.query}%")
                    ilike("lugar", "%${params.query}%")
                }
            }
        }
        params.max = Math.min(params.max ? params.int('max') : 25, 100)
        [params:params, query: params.query, seguroInstanceList: results , seguroInstanceTotal: results?results.size():0]        
    }
    
    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [seguroInstanceList: Seguro.list(params), seguroInstanceTotal: Seguro.count()]
    }

    def create = {
        def seguroInstance = new Seguro()
        seguroInstance.properties = params
        return [seguroInstance: seguroInstance]
    }

    def save = {
        def seguroInstance = new Seguro(params)
        if (seguroInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'seguro.label', default: 'Seguro'), seguroInstance.id])}"
            redirect(action: "show", id: seguroInstance.id)
        }
        else {
            render(view: "create", model: [seguroInstance: seguroInstance])
        }
    }

    def show = {
        def seguroInstance = Seguro.get(params.id)
        if (!seguroInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'seguro.label', default: 'Seguro'), params.id])}"
            redirect(action: "list")
        }
        else {
            [seguroInstance: seguroInstance]
        }
    }

    def edit = {
        def seguroInstance = Seguro.get(params.id)
        if (!seguroInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'seguro.label', default: 'Seguro'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [seguroInstance: seguroInstance]
        }
    }

    def update = {
        def seguroInstance = Seguro.get(params.id)
        if (seguroInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (seguroInstance.version > version) {
                    
                    seguroInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'seguro.label', default: 'Seguro')] as Object[], "Another user has updated this Seguro while you were editing")
                    render(view: "edit", model: [seguroInstance: seguroInstance])
                    return
                }
            }
            seguroInstance.properties = params
            if (!seguroInstance.hasErrors() && seguroInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'seguro.label', default: 'Seguro'), seguroInstance.id])}"
                redirect(action: "show", id: seguroInstance.id)
            }
            else {
                render(view: "edit", model: [seguroInstance: seguroInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'seguro.label', default: 'Seguro'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def seguroInstance = Seguro.get(params.id)
        if (seguroInstance) {
            try {
                seguroInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'seguro.label', default: 'Seguro'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'seguro.label', default: 'Seguro'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'seguro.label', default: 'Seguro'), params.id])}"
            redirect(action: "list")
        }
    }
}
