package hola.documental

import org.junit.*
import grails.test.mixin.*

@TestFor(EdicionesController)
@Mock(Ediciones)
class EdicionesControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/ediciones/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.edicionesInstanceList.size() == 0
        assert model.edicionesInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.edicionesInstance != null
    }

    void testSave() {
        controller.save()

        assert model.edicionesInstance != null
        assert view == '/ediciones/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/ediciones/show/1'
        assert controller.flash.message != null
        assert Ediciones.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/ediciones/list'

        populateValidParams(params)
        def ediciones = new Ediciones(params)

        assert ediciones.save() != null

        params.id = ediciones.id

        def model = controller.show()

        assert model.edicionesInstance == ediciones
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/ediciones/list'

        populateValidParams(params)
        def ediciones = new Ediciones(params)

        assert ediciones.save() != null

        params.id = ediciones.id

        def model = controller.edit()

        assert model.edicionesInstance == ediciones
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/ediciones/list'

        response.reset()

        populateValidParams(params)
        def ediciones = new Ediciones(params)

        assert ediciones.save() != null

        // test invalid parameters in update
        params.id = ediciones.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/ediciones/edit"
        assert model.edicionesInstance != null

        ediciones.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/ediciones/show/$ediciones.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        ediciones.clearErrors()

        populateValidParams(params)
        params.id = ediciones.id
        params.version = -1
        controller.update()

        assert view == "/ediciones/edit"
        assert model.edicionesInstance != null
        assert model.edicionesInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/ediciones/list'

        response.reset()

        populateValidParams(params)
        def ediciones = new Ediciones(params)

        assert ediciones.save() != null
        assert Ediciones.count() == 1

        params.id = ediciones.id

        controller.delete()

        assert Ediciones.count() == 0
        assert Ediciones.get(ediciones.id) == null
        assert response.redirectedUrl == '/ediciones/list'
    }
}
